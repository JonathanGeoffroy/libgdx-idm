[comment encoding = UTF-8 /]

[**
 * Generate an actor and its overrided Libgdx Actor methods
 * Implements show, draw, act, and dispose Libgdx methods
 * For each method, we can add generated lines by using {xxx}Content, where xxx is the name of the libgdx method (for example drawContent)
 * Very useful for Grid2DActor and PixelActor (please see generateGames)
 *  
*/]
[module generateActors('http:///LibGdxMM.ecore')]
[import LibGdx_generation::main::generatePackages /]
[import LibGdx_generation::main::actors::generateTable /]
[import LibGdx_generation::main::actors::generateGames /]
[import LibGdx_generation::main::actors::generateGridActors /]
[import LibGdx_generation::main::actors::generatePixelActors /]

[**
 * Generate an Actor class an override show, draw, act &dispose LibGDX method
 * we can add generated lines on each method by using {xxx}Content method, where xxx is the name of a method (for example drawMethod) 
*/]
[template public generateActor(actor : Actor, screen : Screen, game : Game)]
[file (getPathPackage(game, actor), false, 'UTF-8')]
[generatePackage(game, actor)/]

[actor.importPackage(screen, game)/]

public class [actor.name/] extends [actor.extendedActor()/] {
	[generateActorContent(actor, screen)/]
}
[/file]
[/template]

[template public generateActorContent(actor : Actor, screen : Screen)/]
[template public generateActorContent(actor : GameActor, screen : Screen)]
[actor.actorAttributes(screen)/]

[actor.actorConstructor(screen)/]

[actor.showMethod()/]

[actor.drawMethod()/]

[actor.actMethod()/]

[actor.disposeMethod()/]	

[actor.collisionsMethod()/]
[/template]

[template public generateActorContent(actor : Table, screen : Screen)]
[actor.actorAttributes(screen)/]

[actor.actorConstructor(screen)/]

[actor.showMethod()/]

[actor.drawMethod()/]

[actor.actMethod()/]

[actor.disposeMethod()/]
[/template]

[**
 * Create attributes of this Actors
*/]
[template public actorAttributes(actor : Actor, screen : Screen)]
private [screen.name/] screen;
[actor.attributes()/]
[/template]

[**
 * Add specific attributes for each actor, depending on its type
*/]
[template public attributes(actor : Actor)/]

[**
 * Create the constructor of this actor
*/]
[template public actorConstructor(actor : Actor, screen : Screen)]
public [actor.name/] (final [screen.name/] screen) {
	[actor.constructor()/]
	this.screen = screen;
}
[/template]

[**
 * Add specific generated lines for each actor's contructor, depending on its type
*/]
[template public constructor(actor : Actor)/]

[**
 * call  {Role}.init() for each Role type used in this Actor 
*/]
[template private initRoles(actor : Actor)/]

[**
 * Instantiate all entities of this actor
*/]
[template private createEntities(actor : Actor)/]

[**
 * Generate the show method 
 * Each Acto's type can add specific show lines by define showContent() template 
*/]
[template public showMethod(actor : Actor)]
public void show() {
	[actor.showContent()/]
}
[/template]

[template public showContent(actor : Actor)/]

[**
 * Generate the draw method 
 * Each Acto's type can add specific lines by define drawContent() template 
*/]
[template public drawMethod(actor : Actor)]
public void draw(SpriteBatch batch, float parentAlpha) {
	super.draw(batch, parentAlpha);
	[actor.drawContent()/]
}
[/template]

[template public drawContent(actor : Actor)/]


[**
 * Generate the act method 
 * Each Acto's type can add specific lines by define actContent() template 
*/]
[template public actMethod(actor : Actor)]
public void act(float deltaTime) {
	super.act(deltaTime);
	[actor.actContent()/]
}
[/template]

[template public actContent(actor : Actor)/]

[**
 * Generate the dispose method 
 * Each Acto's type can add specific lines by define disposeContent() template 
*/]
[template public disposeMethod(actor : Actor)]
public void dispose() {
	[actor.disposeContent()/]
}
[/template]

[template public disposeContent(actor : Actor)/]


[**
 * Compute the position of this actor, where it is at the Center position
*/]
[template public pos(actor : Actor) ? (actor.position = PositionEnum::Center)]
// CENTER
currentActor.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
[/template]

[**
 * Compute the position of this actor, where it is at the North position
*/]
[template public pos(actor : Actor) ? (actor.position = PositionEnum::North)]
// NORTH
currentActor.setBounds(0, Gdx.graphics.getHeight() * 0.9f, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() / 10.f);
[/template]

[**
 * Compute the position of this actor, where it is at the South position
*/]
[template public pos(actor : Actor) ? (actor.position = PositionEnum::South)]
// SOUTH
currentActor.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()/10.f);
[/template]

[**
 * Compute the position of this actor, where it is at the West position
*/]
[template public pos(actor : Actor) ? (actor.position = PositionEnum::West)]
// WEST
currentActor.setBounds(0, 0, Gdx.graphics.getWidth() / 10.f, Gdx.graphics.getHeight());
[/template]

[**
 * Compute the position of this actor, where it is at the East position
*/]
[template public pos(actor : Actor) ? (actor.position = PositionEnum::East)]
// EAST
currentActor.setBounds(0, Gdx.graphics.getWidth() * 0.9f, Gdx.graphics.getWidth() / 10.f, Gdx.graphics.getHeight());
[/template]


[template private extendedActor(actor : Actor)]!!! Error : no Actor extends available !!![/template]
[template private extendedActor(actor : PixelActor)]EngineActor[/template]
[template private extendedActor(actor : Grid2DActor)]GridEngineActor[/template]
[template private extendedActor(actor : Table)]Table[/template]
