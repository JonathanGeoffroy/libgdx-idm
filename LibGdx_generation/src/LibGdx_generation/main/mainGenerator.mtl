[comment encoding = UTF-8 /]
[**
 * Main Generator of the project
 * This generator creates project, all Screens, all Roles, and all Actors for each Screen.
 * -->  Project
 *      ---> Screens
 *      	---> Actors
 *      		---> Entities (Role instantiation)
 *      ---> Role
*/]
[module mainGenerator('http:///LibGdxMM.ecore')/]
[import LibGdx_generation::main::generatePackages /]
[import LibGdx_generation::main::screens::generateScreens /]
[import LibGdx_generation::main::actors::generateActors /]
[import LibGdx_generation::main::actors::generateTable /]
[import LibGdx_generation::main::actors::generateGames /]
[import LibGdx_generation::main::roles::generateRoles /]
[import LibGdx_generation::main::entities::generateCollisions /]

[template public mainGenerator(game: Game)]
[comment @main /]
[game.createMainClass()/]

[comment][game.createSkin()/][/comment]

[game.createEntityAbstraction()/]
[game.createPixelEntityAbstraction()/]
[game.createGridEntityAbstraction()/]

[game.createEngineAbstraction()/]
[game.createGridEngineAbstraction()/]

[game.createVisitorInterface()/]
[game.createAcceptInterface()/]

[for (screen : Screen | game.screens)]
	[screen.generateScreen(game)/]

	[for (actor : Actor | screen.actors)]
		[actor.generateActor(screen, game)/]
	[/for]
[/for]

[for (role : Role | game.roles)]
	[role.generateRole(game)/]
[/for]
[/template]

[**
 * Generate the main class which run the app by calling the first screen of a project
*/]
[template public createMainClass(game: Game)]
[file (getPathPackage(game), false, 'UTF-8')]
[game.generatePackage()/]
[game.importPackage()/]
public class [game.name/] extends Game {
	@Override
	public void create() {
		setScreen(new [game.screens->asSequence()->first().name/](this));
	}
}
[/file]
[/template]

[**
 * Create Entity interface
*/]

[template public createEntityAbstraction(game : Game)]
[file(defaultPackage(game) + 'Entity.java', false, 'UTF-8')]
[game.generatePackage()/]

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import [game.gamePackage/].roles.collisions.*;

public abstract class Entity extends Image implements Visitor, Acceptable {
	public Entity(TextureRegion reg) {
		super(reg);
	}

	public void move() {};

	[game.collisionMethod()/]
}
[/file]
[/template]

[template public createPixelEntityAbstraction(game : Game)]
[file(defaultPackage(game) + 'PixelEntity.java', false, 'UTF-8')]
[game.generatePackage()/]

public abstract class PixelEntity extends Entity {
	public PixelEntity(TextureRegion reg) {
		super(reg);
	}
}
[/file]
[/template]

[**
 * Create GridEntity interface
*/]
[template public createGridEntityAbstraction(game : Game)]
[file(defaultPackage(game) + 'GridEntity.java', false, 'UTF-8')]
[game.generatePackage()/]

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public abstract class GridEntity extends Entity {
	public GridEntity(TextureRegion reg) {
		super(reg);
	}

	public abstract boolean canMoveAt(GridEntity['['/]]['['/]] grid, int fromX, int fromY, int toX, int toY);

	/**
  	  * Create a new Listener for Drag events
  	*/
	public DragListener dragListener(final GridEngineActor act) {
		DragListener onDrag = new DragListener() {
			private boolean dragged;
			private GridEngineActor actor = act;
			private float fromX, fromY;

			@Override
			public void dragStart(InputEvent event, float x, float y,
					int pointer) {
				super.dragStart(event, x, y, pointer);
				if (x >= getX() && x <= getX() + getWidth() &&
					y >= getY() && y <= getY() + getHeight()) {
					dragged = true;
					fromX = x;
					fromY = y;
				}
			}
	
			@Override
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				super.dragStop(event, x, y, pointer);
				if(dragged) {
					dragged = false;
					actor.moveEntity(fromX, fromY, x, y);
				}
			}
			
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				if (dragged) {
					setPosition(x - getWidth() / 2, y - getHeight() /2);
				}
			}		
		};
		return onDrag;
	}
} 
[/file]
[/template]

[template public collisionMethod(game : Game)]
public boolean overlaps(Entity other) {
	return getX() < other.getX() + other.getWidth() && 
			getX() + getWidth() > other.getX() && 
			getY() < other.getY() + other.getHeight() && 
			getY() + getHeight() > other.getY();
}
[/template]

[template public createEngineAbstraction(game : Game)]
[file(defaultPackage(game) + '/actors/EngineActor.java', false, 'UTF-8')]
[game.generatePackage().replace(';', '') + '.actors;'/]
public abstract class EngineActor extends Actor {
	protected Game game;

	/**
	 * All entities in the screen
	 */
	protected List<Entity> entities;
	
	public EngineActor(Game game) {	
		super();
		this.game = game;
		entities = new ArrayList<Entity>();
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public void removeEntity(Entity entity) {
		entities.remove(entity);
	}

	public Game getGame() {
		return game;
	}
}
[/file]
[/template]

[template public createGridEngineAbstraction(game : Game)]
[file(defaultPackage(game) + '/actors/GridEngineActor.java', false, 'UTF-8')]
[game.generatePackage().replace(';', '') + '.actors;'/]
public abstract class GridEngineActor extends EngineActor {
	/**
	  * A 2D grid, containing all entities
	  * A Case can contains only 1 entity, or can be null if there is no entity at this case
	  */
	protected GridEntity['[][]'/] gridEntities;

	public GridEngineActor(Game game, int sizeX, int sizeY) {
		super(game);
		gridEntities = new GridEntity['['/]sizeY]['['/]sizeX];
	}

	@Override
	public void removeEntity(Entity entity) {
		entities.remove(entity);
		for(int i = 0; i < gridEntities.length; i++) {
			for(int j = 0; j < gridEntities['['/]i].length; j++) {
				if(gridEntities['['/]i]['['/]j].equals(entity)) {
					gridEntities['['/]i]['['/]j] = null;
					return;
				}
			}
		}
	}

	public void setEntityAt(GridEntity entity, int abs, int ord) {
		gridEntities['['/]ord]['['/]abs] = entity;
	}

	public void moveEntity(float pixFromX, float pixFromY, float pixToX, float pixToY) {
		float caseWidth = getWidth() / gridEntities['['/]0].length;
		float caseHeight = getHeight() / gridEntities.length;
		int fromY = gridEntities.length - (int) (pixFromY / caseHeight) - 1;
		int fromX = (int) (pixFromX / caseWidth);
		int toY = gridEntities.length - (int) (pixToY / caseHeight) - 1;
		int toX = (int) (pixToX / caseWidth);
		GridEntity movedEntity = gridEntities['['/]fromY]['['/]fromX];
		
		if(movedEntity.canMoveAt(gridEntities, fromX, fromY, toX, toY)) {
			// Remove entity at the toX-toY place
			if(gridEntities['['/]toY]['['/]toX] != null) {
				entities.remove(gridEntities['['/]toY]['['/]toX]);
				gridEntities['['/]toY]['['/]toX].setPosition(9999, 9999);
				gridEntities['['/]toY]['['/]toX] = null;
			}
			// Move entity at toX-toY
			gridEntities['['/]fromY]['['/]fromX] = null;
			gridEntities['['/]toY]['['/]toX] = movedEntity;
			movedEntity.setPosition(toX * caseWidth, getHeight() - (toY+1) * caseHeight);
		}
		else {
			// Replace entity a it old place
			movedEntity.setPosition(fromX * caseWidth, getHeight() - (fromY+1) * caseHeight);
		}
	} 
}
[/file]
[/template]