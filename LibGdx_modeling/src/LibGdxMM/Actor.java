/**
 */
package LibGdxMM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Découpage d'un Screen en plusieurs partie
 * Chaque Actor gère son propre affichage et ses entities qui lui sont propres
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link LibGdxMM.Actor#getPosition <em>Position</em>}</li>
 * </ul>
 * </p>
 *
 * @see LibGdxMM.LibGdxMMPackage#getActor()
 * @model abstract="true"
 * @generated
 */
public interface Actor extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Position</b></em>' attribute.
	 * The literals are from the enumeration {@link LibGdxMM.PositionEnum}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Position</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Position</em>' attribute.
	 * @see LibGdxMM.PositionEnum
	 * @see #setPosition(PositionEnum)
	 * @see LibGdxMM.LibGdxMMPackage#getActor_Position()
	 * @model unique="false" required="true" ordered="false"
	 * @generated
	 */
	PositionEnum getPosition();

	/**
	 * Sets the value of the '{@link LibGdxMM.Actor#getPosition <em>Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Position</em>' attribute.
	 * @see LibGdxMM.PositionEnum
	 * @see #getPosition()
	 * @generated
	 */
	void setPosition(PositionEnum value);

} // Actor
