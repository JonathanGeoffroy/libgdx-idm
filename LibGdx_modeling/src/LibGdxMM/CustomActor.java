/**
 */
package LibGdxMM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Custom Actor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see LibGdxMM.LibGdxMMPackage#getCustomActor()
 * @model
 * @generated
 */
public interface CustomActor extends Actor {
} // CustomActor
