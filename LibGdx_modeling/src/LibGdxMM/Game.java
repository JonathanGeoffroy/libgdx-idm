/**
 */
package LibGdxMM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Game</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Projet principal à générer
 * 
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link LibGdxMM.Game#getScreens <em>Screens</em>}</li>
 *   <li>{@link LibGdxMM.Game#getGamePackage <em>Game Package</em>}</li>
 *   <li>{@link LibGdxMM.Game#getRoles <em>Roles</em>}</li>
 *   <li>{@link LibGdxMM.Game#getSdkLocation <em>Sdk Location</em>}</li>
 *   <li>{@link LibGdxMM.Game#getResource <em>Resource</em>}</li>
 * </ul>
 * </p>
 *
 * @see LibGdxMM.LibGdxMMPackage#getGame()
 * @model
 * @generated
 */
public interface Game extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Screens</b></em>' containment reference list.
	 * The list contents are of type {@link LibGdxMM.Screen}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Screens</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Screens</em>' containment reference list.
	 * @see LibGdxMM.LibGdxMMPackage#getGame_Screens()
	 * @model containment="true"
	 * @generated
	 */
	EList<Screen> getScreens();

	/**
	 * Returns the value of the '<em><b>Game Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Game Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Game Package</em>' attribute.
	 * @see #setGamePackage(String)
	 * @see LibGdxMM.LibGdxMMPackage#getGame_GamePackage()
	 * @model dataType="LibGdxMM.String" required="true" ordered="false"
	 * @generated
	 */
	Object getGamePackage();

	/**
	 * Sets the value of the '{@link LibGdxMM.Game#getGamePackage <em>Game Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Game Package</em>' attribute.
	 * @see #getGamePackage()
	 * @generated
	 */
	void setGamePackage(Object value);

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link LibGdxMM.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see LibGdxMM.LibGdxMMPackage#getGame_Roles()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Role> getRoles();

	/**
	 * Returns the value of the '<em><b>Sdk Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sdk Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sdk Location</em>' attribute.
	 * @see #setSdkLocation(String)
	 * @see LibGdxMM.LibGdxMMPackage#getGame_SdkLocation()
	 * @model unique="false" dataType="LibGdxMM.String" required="true" ordered="false"
	 * @generated
	 */
	Object getSdkLocation();

	/**
	 * Sets the value of the '{@link LibGdxMM.Game#getSdkLocation <em>Sdk Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sdk Location</em>' attribute.
	 * @see #getSdkLocation()
	 * @generated
	 */
	void setSdkLocation(Object value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' containment reference list.
	 * The list contents are of type {@link LibGdxMM.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' containment reference list.
	 * @see LibGdxMM.LibGdxMMPackage#getGame_Resource()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Resource> getResource();

} // Game
