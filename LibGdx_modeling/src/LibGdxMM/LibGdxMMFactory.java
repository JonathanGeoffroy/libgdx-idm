/**
 */
package LibGdxMM;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see LibGdxMM.LibGdxMMPackage
 * @generated
 */
public interface LibGdxMMFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LibGdxMMFactory eINSTANCE = LibGdxMM.impl.LibGdxMMFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Game</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Game</em>'.
	 * @generated
	 */
	Game createGame();

	/**
	 * Returns a new object of class '<em>Screen</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Screen</em>'.
	 * @generated
	 */
	Screen createScreen();

	/**
	 * Returns a new object of class '<em>Image</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Image</em>'.
	 * @generated
	 */
	Image createImage();

	/**
	 * Returns a new object of class '<em>Music</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Music</em>'.
	 * @generated
	 */
	Music createMusic();

	/**
	 * Returns a new object of class '<em>Pixel Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pixel Action</em>'.
	 * @generated
	 */
	PixelAction createPixelAction();

	/**
	 * Returns a new object of class '<em>Act Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Act Action</em>'.
	 * @generated
	 */
	ActAction createActAction();

	/**
	 * Returns a new object of class '<em>Collision</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Collision</em>'.
	 * @generated
	 */
	Collision createCollision();

	/**
	 * Returns a new object of class '<em>Custom Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Custom Actor</em>'.
	 * @generated
	 */
	CustomActor createCustomActor();

	/**
	 * Returns a new object of class '<em>Role Factory</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Role Factory</em>'.
	 * @generated
	 */
	RoleFactory createRoleFactory();

	/**
	 * Returns a new object of class '<em>Status Bar</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Status Bar</em>'.
	 * @generated
	 */
	StatusBar createStatusBar();

	/**
	 * Returns a new object of class '<em>Button</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Button</em>'.
	 * @generated
	 */
	Button createButton();

	/**
	 * Returns a new object of class '<em>Label</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Label</em>'.
	 * @generated
	 */
	Label createLabel();

	/**
	 * Returns a new object of class '<em>Image Widget</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Image Widget</em>'.
	 * @generated
	 */
	ImageWidget createImageWidget();

	/**
	 * Returns a new object of class '<em>Next Screen Button</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Next Screen Button</em>'.
	 * @generated
	 */
	NextScreenButton createNextScreenButton();

	/**
	 * Returns a new object of class '<em>Ennemy Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Ennemy Instance</em>'.
	 * @generated
	 */
	EnnemyInstance createEnnemyInstance();

	/**
	 * Returns a new object of class '<em>Real Time Engine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Real Time Engine</em>'.
	 * @generated
	 */
	RealTimeEngine createRealTimeEngine();

	/**
	 * Returns a new object of class '<em>Turn Based Engine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Turn Based Engine</em>'.
	 * @generated
	 */
	TurnBasedEngine createTurnBasedEngine();

	/**
	 * Returns a new object of class '<em>Grid2 DActor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Grid2 DActor</em>'.
	 * @generated
	 */
	Grid2DActor createGrid2DActor();

	/**
	 * Returns a new object of class '<em>Pixel Actor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pixel Actor</em>'.
	 * @generated
	 */
	PixelActor createPixelActor();

	/**
	 * Returns a new object of class '<em>Pixel Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pixel Entity</em>'.
	 * @generated
	 */
	PixelEntity createPixelEntity();

	/**
	 * Returns a new object of class '<em>Pixel Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Pixel Role</em>'.
	 * @generated
	 */
	PixelRole createPixelRole();

	/**
	 * Returns a new object of class '<em>Grid Entity</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Grid Entity</em>'.
	 * @generated
	 */
	GridEntity createGridEntity();

	/**
	 * Returns a new object of class '<em>Grid Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Grid Role</em>'.
	 * @generated
	 */
	GridRole createGridRole();

	/**
	 * Returns a new object of class '<em>Add Grid Role</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Add Grid Role</em>'.
	 * @generated
	 */
	AddGridRole createAddGridRole();

	/**
	 * Returns a new object of class '<em>Table</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Table</em>'.
	 * @generated
	 */
	Table createTable();

	/**
	 * Returns a new object of class '<em>Destroy Collision</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Destroy Collision</em>'.
	 * @generated
	 */
	DestroyCollision createDestroyCollision();

	/**
	 * Returns a new object of class '<em>Change Screen Collision</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Change Screen Collision</em>'.
	 * @generated
	 */
	ChangeScreenCollision createChangeScreenCollision();

	/**
	 * Returns a new object of class '<em>Custom Collision</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Custom Collision</em>'.
	 * @generated
	 */
	CustomCollision createCustomCollision();

	/**
	 * Returns a new object of class '<em>Quit Button</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Quit Button</em>'.
	 * @generated
	 */
	QuitButton createQuitButton();

	/**
	 * Returns a new object of class '<em>Horizontal Moving</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Horizontal Moving</em>'.
	 * @generated
	 */
	HorizontalMoving createHorizontalMoving();

	/**
	 * Returns a new object of class '<em>Vertical Moving</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vertical Moving</em>'.
	 * @generated
	 */
	VerticalMoving createVerticalMoving();

	/**
	 * Returns a new object of class '<em>Diagonal Moving</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Diagonal Moving</em>'.
	 * @generated
	 */
	DiagonalMoving createDiagonalMoving();

	/**
	 * Returns a new object of class '<em>Custom Moving</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Custom Moving</em>'.
	 * @generated
	 */
	CustomMoving createCustomMoving();

	/**
	 * Returns a new object of class '<em>Font</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Font</em>'.
	 * @generated
	 */
	Font createFont();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	LibGdxMMPackage getLibGdxMMPackage();

} //LibGdxMMFactory
