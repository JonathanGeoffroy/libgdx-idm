/**
 */
package LibGdxMM;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see LibGdxMM.LibGdxMMFactory
 * @model kind="package"
 * @generated
 */
public interface LibGdxMMPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "LibGdxMM";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///LibGdxMM.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "LibGdxMM";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	LibGdxMMPackage eINSTANCE = LibGdxMM.impl.LibGdxMMPackageImpl.init();

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ElementImpl <em>Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ElementImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getElement()
	 * @generated
	 */
	int ELEMENT = 2;

	/**
	 * The number of structural features of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.NamedElementImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.GameImpl <em>Game</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.GameImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGame()
	 * @generated
	 */
	int GAME = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Screens</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME__SCREENS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Game Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME__GAME_PACKAGE = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME__ROLES = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Sdk Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME__SDK_LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME__RESOURCE = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Game</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Game</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ScreenImpl <em>Screen</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ScreenImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getScreen()
	 * @generated
	 */
	int SCREEN = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCREEN__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Actors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCREEN__ACTORS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Wallpaper</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCREEN__WALLPAPER = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Music</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCREEN__MUSIC = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Screen</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCREEN_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Screen</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCREEN_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ActorImpl <em>Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ActorImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getActor()
	 * @generated
	 */
	int ACTOR = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR__POSITION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTOR_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ResourceImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 6;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__PATH = 0;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ImageImpl <em>Image</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ImageImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getImage()
	 * @generated
	 */
	int IMAGE = 5;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__PATH = RESOURCE__PATH;

	/**
	 * The feature id for the '<em><b>Real Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__REAL_WIDTH = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Real Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE__REAL_HEIGHT = RESOURCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Image</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_OPERATION_COUNT = RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.MusicImpl <em>Music</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.MusicImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getMusic()
	 * @generated
	 */
	int MUSIC = 7;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSIC__PATH = RESOURCE__PATH;

	/**
	 * The number of structural features of the '<em>Music</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSIC_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Music</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MUSIC_OPERATION_COUNT = RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.RoleImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__IMAGE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Pixel Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__PIXEL_ACTION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Act Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__ACT_ACTION = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Collision</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__COLLISION = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ActionImpl <em>Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ActionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getAction()
	 * @generated
	 */
	int ACTION = 10;

	/**
	 * The number of structural features of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_OPERATION_COUNT = ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.PixelActionImpl <em>Pixel Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.PixelActionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelAction()
	 * @generated
	 */
	int PIXEL_ACTION = 9;

	/**
	 * The feature id for the '<em><b>Drag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTION__DRAG = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Click</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTION__CLICK = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Pixel Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Pixel Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ActActionImpl <em>Act Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ActActionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getActAction()
	 * @generated
	 */
	int ACT_ACTION = 11;

	/**
	 * The feature id for the '<em><b>Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACT_ACTION__ACTION = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Act Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACT_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Act Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACT_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.CollisionImpl <em>Collision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.CollisionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCollision()
	 * @generated
	 */
	int COLLISION = 12;

	/**
	 * The feature id for the '<em><b>With</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLISION__WITH = ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Collision Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLISION__COLLISION_TYPE = ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLISION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLISION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.CollisionTypeImpl <em>Collision Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.CollisionTypeImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCollisionType()
	 * @generated
	 */
	int COLLISION_TYPE = 13;

	/**
	 * The number of structural features of the '<em>Collision Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLISION_TYPE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Collision Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COLLISION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.CustomActorImpl <em>Custom Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.CustomActorImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCustomActor()
	 * @generated
	 */
	int CUSTOM_ACTOR = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ACTOR__NAME = ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ACTOR__POSITION = ACTOR__POSITION;

	/**
	 * The number of structural features of the '<em>Custom Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ACTOR_FEATURE_COUNT = ACTOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Custom Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_ACTOR_OPERATION_COUNT = ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.GameActorImpl <em>Game Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.GameActorImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGameActor()
	 * @generated
	 */
	int GAME_ACTOR = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_ACTOR__NAME = ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_ACTOR__POSITION = ACTOR__POSITION;

	/**
	 * The feature id for the '<em><b>Role Factory</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_ACTOR__ROLE_FACTORY = ACTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_ACTOR__ENGINE = ACTOR_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Game Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_ACTOR_FEATURE_COUNT = ACTOR_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Game Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GAME_ACTOR_OPERATION_COUNT = ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.RoleFactoryImpl <em>Role Factory</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.RoleFactoryImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getRoleFactory()
	 * @generated
	 */
	int ROLE_FACTORY = 16;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY__ROLE = 0;

	/**
	 * The feature id for the '<em><b>Time To Create</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY__TIME_TO_CREATE = 1;

	/**
	 * The feature id for the '<em><b>Slack Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY__SLACK_TIME = 2;

	/**
	 * The feature id for the '<em><b>XCreated Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY__XCREATED_POSITION = 3;

	/**
	 * The feature id for the '<em><b>YCreated Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY__YCREATED_POSITION = 4;

	/**
	 * The number of structural features of the '<em>Role Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Role Factory</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FACTORY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.EngineImpl <em>Engine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.EngineImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getEngine()
	 * @generated
	 */
	int ENGINE = 17;

	/**
	 * The number of structural features of the '<em>Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENGINE_FEATURE_COUNT = ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENGINE_OPERATION_COUNT = ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.StatusBarImpl <em>Status Bar</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.StatusBarImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getStatusBar()
	 * @generated
	 */
	int STATUS_BAR = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_BAR__NAME = ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_BAR__POSITION = ACTOR__POSITION;

	/**
	 * The number of structural features of the '<em>Status Bar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_BAR_FEATURE_COUNT = ACTOR_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Status Bar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STATUS_BAR_OPERATION_COUNT = ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.WidgetImpl <em>Widget</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.WidgetImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getWidget()
	 * @generated
	 */
	int WIDGET = 19;

	/**
	 * The number of structural features of the '<em>Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIDGET_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WIDGET_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.TextualWidgetImpl <em>Textual Widget</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.TextualWidgetImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getTextualWidget()
	 * @generated
	 */
	int TEXTUAL_WIDGET = 21;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXTUAL_WIDGET__TEXT = WIDGET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Textual Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXTUAL_WIDGET_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Textual Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEXTUAL_WIDGET_OPERATION_COUNT = WIDGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ButtonImpl <em>Button</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ButtonImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getButton()
	 * @generated
	 */
	int BUTTON = 20;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON__TEXT = TEXTUAL_WIDGET__TEXT;

	/**
	 * The number of structural features of the '<em>Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON_FEATURE_COUNT = TEXTUAL_WIDGET_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUTTON_OPERATION_COUNT = TEXTUAL_WIDGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.LabelImpl <em>Label</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.LabelImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getLabel()
	 * @generated
	 */
	int LABEL = 22;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL__TEXT = TEXTUAL_WIDGET__TEXT;

	/**
	 * The number of structural features of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_FEATURE_COUNT = TEXTUAL_WIDGET_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Label</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LABEL_OPERATION_COUNT = TEXTUAL_WIDGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ImageWidgetImpl <em>Image Widget</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ImageWidgetImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getImageWidget()
	 * @generated
	 */
	int IMAGE_WIDGET = 23;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_WIDGET__IMAGE = WIDGET_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Image Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_WIDGET_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Image Widget</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_WIDGET_OPERATION_COUNT = WIDGET_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.NextScreenButtonImpl <em>Next Screen Button</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.NextScreenButtonImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getNextScreenButton()
	 * @generated
	 */
	int NEXT_SCREEN_BUTTON = 24;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_SCREEN_BUTTON__TEXT = BUTTON__TEXT;

	/**
	 * The feature id for the '<em><b>Next Screen</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_SCREEN_BUTTON__NEXT_SCREEN = BUTTON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Next Screen Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_SCREEN_BUTTON_FEATURE_COUNT = BUTTON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Next Screen Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_SCREEN_BUTTON_OPERATION_COUNT = BUTTON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.EnnemyInstanceImpl <em>Ennemy Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.EnnemyInstanceImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getEnnemyInstance()
	 * @generated
	 */
	int ENNEMY_INSTANCE = 25;

	/**
	 * The number of structural features of the '<em>Ennemy Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENNEMY_INSTANCE_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Ennemy Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENNEMY_INSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.RealTimeEngineImpl <em>Real Time Engine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.RealTimeEngineImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getRealTimeEngine()
	 * @generated
	 */
	int REAL_TIME_ENGINE = 26;

	/**
	 * The number of structural features of the '<em>Real Time Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_ENGINE_FEATURE_COUNT = ENGINE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Real Time Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REAL_TIME_ENGINE_OPERATION_COUNT = ENGINE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.TurnBasedEngineImpl <em>Turn Based Engine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.TurnBasedEngineImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getTurnBasedEngine()
	 * @generated
	 */
	int TURN_BASED_ENGINE = 27;

	/**
	 * The number of structural features of the '<em>Turn Based Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_BASED_ENGINE_FEATURE_COUNT = ENGINE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Turn Based Engine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TURN_BASED_ENGINE_OPERATION_COUNT = ENGINE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.PixelActorImpl <em>Pixel Actor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.PixelActorImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelActor()
	 * @generated
	 */
	int PIXEL_ACTOR = 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR__NAME = GAME_ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR__POSITION = GAME_ACTOR__POSITION;

	/**
	 * The feature id for the '<em><b>Role Factory</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR__ROLE_FACTORY = GAME_ACTOR__ROLE_FACTORY;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR__ENGINE = GAME_ACTOR__ENGINE;

	/**
	 * The feature id for the '<em><b>Pixel Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR__PIXEL_ENTITIES = GAME_ACTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Pixel Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR_FEATURE_COUNT = GAME_ACTOR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Pixel Actor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ACTOR_OPERATION_COUNT = GAME_ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.Grid2DActorImpl <em>Grid2 DActor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.Grid2DActorImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGrid2DActor()
	 * @generated
	 */
	int GRID2_DACTOR = 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__NAME = PIXEL_ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__POSITION = PIXEL_ACTOR__POSITION;

	/**
	 * The feature id for the '<em><b>Role Factory</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__ROLE_FACTORY = PIXEL_ACTOR__ROLE_FACTORY;

	/**
	 * The feature id for the '<em><b>Engine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__ENGINE = PIXEL_ACTOR__ENGINE;

	/**
	 * The feature id for the '<em><b>Pixel Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__PIXEL_ENTITIES = PIXEL_ACTOR__PIXEL_ENTITIES;

	/**
	 * The feature id for the '<em><b>Grid Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__GRID_ENTITIES = PIXEL_ACTOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Grid Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__GRID_WIDTH = PIXEL_ACTOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Grid Height</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__GRID_HEIGHT = PIXEL_ACTOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Grid Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__GRID_COLOR = PIXEL_ACTOR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Add Grid Role</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR__ADD_GRID_ROLE = PIXEL_ACTOR_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Grid2 DActor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR_FEATURE_COUNT = PIXEL_ACTOR_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Grid2 DActor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID2_DACTOR_OPERATION_COUNT = PIXEL_ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.EntityImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 31;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.PixelEntityImpl <em>Pixel Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.PixelEntityImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelEntity()
	 * @generated
	 */
	int PIXEL_ENTITY = 30;

	/**
	 * The feature id for the '<em><b>XPosition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ENTITY__XPOSITION = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>YPosition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ENTITY__YPOSITION = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ENTITY__ROLE = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Pixel Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ENTITY_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Pixel Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ENTITY_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.PixelRoleImpl <em>Pixel Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.PixelRoleImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelRole()
	 * @generated
	 */
	int PIXEL_ROLE = 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE__NAME = ROLE__NAME;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE__IMAGE = ROLE__IMAGE;

	/**
	 * The feature id for the '<em><b>Pixel Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE__PIXEL_ACTION = ROLE__PIXEL_ACTION;

	/**
	 * The feature id for the '<em><b>Act Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE__ACT_ACTION = ROLE__ACT_ACTION;

	/**
	 * The feature id for the '<em><b>Collision</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE__COLLISION = ROLE__COLLISION;

	/**
	 * The number of structural features of the '<em>Pixel Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE_FEATURE_COUNT = ROLE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Pixel Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIXEL_ROLE_OPERATION_COUNT = ROLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.GridEntityImpl <em>Grid Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.GridEntityImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGridEntity()
	 * @generated
	 */
	int GRID_ENTITY = 33;

	/**
	 * The feature id for the '<em><b>Abs Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ENTITY__ABS_POSITION = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Ord Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ENTITY__ORD_POSITION = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ENTITY__ROLE = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Grid Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ENTITY_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Grid Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ENTITY_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.GridRoleImpl <em>Grid Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.GridRoleImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGridRole()
	 * @generated
	 */
	int GRID_ROLE = 34;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE__NAME = ROLE__NAME;

	/**
	 * The feature id for the '<em><b>Image</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE__IMAGE = ROLE__IMAGE;

	/**
	 * The feature id for the '<em><b>Pixel Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE__PIXEL_ACTION = ROLE__PIXEL_ACTION;

	/**
	 * The feature id for the '<em><b>Act Action</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE__ACT_ACTION = ROLE__ACT_ACTION;

	/**
	 * The feature id for the '<em><b>Collision</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE__COLLISION = ROLE__COLLISION;

	/**
	 * The feature id for the '<em><b>Move Grid Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE__MOVE_GRID_ACTIONS = ROLE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Grid Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE_FEATURE_COUNT = ROLE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Grid Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ROLE_OPERATION_COUNT = ROLE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.GridActionImpl <em>Grid Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.GridActionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGridAction()
	 * @generated
	 */
	int GRID_ACTION = 36;

	/**
	 * The number of structural features of the '<em>Grid Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ACTION_FEATURE_COUNT = ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Grid Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GRID_ACTION_OPERATION_COUNT = ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.MoveGridActionImpl <em>Move Grid Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.MoveGridActionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getMoveGridAction()
	 * @generated
	 */
	int MOVE_GRID_ACTION = 35;

	/**
	 * The feature id for the '<em><b>Nb Cases Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_GRID_ACTION__NB_CASES_MIN = GRID_ACTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nb Cases Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_GRID_ACTION__NB_CASES_MAX = GRID_ACTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Can Jump Other Entities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES = GRID_ACTION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Move Grid Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_GRID_ACTION_FEATURE_COUNT = GRID_ACTION_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Move Grid Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOVE_GRID_ACTION_OPERATION_COUNT = GRID_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.AddGridRoleImpl <em>Add Grid Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.AddGridRoleImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getAddGridRole()
	 * @generated
	 */
	int ADD_GRID_ROLE = 37;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_GRID_ROLE__ROLE = GRID_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Add Grid Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_GRID_ROLE_FEATURE_COUNT = GRID_ACTION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Add Grid Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADD_GRID_ROLE_OPERATION_COUNT = GRID_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.TableImpl <em>Table</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.TableImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getTable()
	 * @generated
	 */
	int TABLE = 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__NAME = ACTOR__NAME;

	/**
	 * The feature id for the '<em><b>Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__POSITION = ACTOR__POSITION;

	/**
	 * The feature id for the '<em><b>Widgets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE__WIDGETS = ACTOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_FEATURE_COUNT = ACTOR_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Table</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TABLE_OPERATION_COUNT = ACTOR_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.DestroyCollisionImpl <em>Destroy Collision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.DestroyCollisionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getDestroyCollision()
	 * @generated
	 */
	int DESTROY_COLLISION = 39;

	/**
	 * The number of structural features of the '<em>Destroy Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESTROY_COLLISION_FEATURE_COUNT = COLLISION_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Destroy Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DESTROY_COLLISION_OPERATION_COUNT = COLLISION_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.ChangeScreenCollisionImpl <em>Change Screen Collision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.ChangeScreenCollisionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getChangeScreenCollision()
	 * @generated
	 */
	int CHANGE_SCREEN_COLLISION = 40;

	/**
	 * The feature id for the '<em><b>Screen</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_SCREEN_COLLISION__SCREEN = COLLISION_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Change Screen Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_SCREEN_COLLISION_FEATURE_COUNT = COLLISION_TYPE_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Change Screen Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHANGE_SCREEN_COLLISION_OPERATION_COUNT = COLLISION_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.CustomCollisionImpl <em>Custom Collision</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.CustomCollisionImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCustomCollision()
	 * @generated
	 */
	int CUSTOM_COLLISION = 41;

	/**
	 * The number of structural features of the '<em>Custom Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_COLLISION_FEATURE_COUNT = COLLISION_TYPE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Custom Collision</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_COLLISION_OPERATION_COUNT = COLLISION_TYPE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.QuitButtonImpl <em>Quit Button</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.QuitButtonImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getQuitButton()
	 * @generated
	 */
	int QUIT_BUTTON = 42;

	/**
	 * The feature id for the '<em><b>Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUIT_BUTTON__TEXT = BUTTON__TEXT;

	/**
	 * The number of structural features of the '<em>Quit Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUIT_BUTTON_FEATURE_COUNT = BUTTON_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Quit Button</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUIT_BUTTON_OPERATION_COUNT = BUTTON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.HorizontalMovingImpl <em>Horizontal Moving</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.HorizontalMovingImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getHorizontalMoving()
	 * @generated
	 */
	int HORIZONTAL_MOVING = 43;

	/**
	 * The feature id for the '<em><b>Nb Cases Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_MOVING__NB_CASES_MIN = MOVE_GRID_ACTION__NB_CASES_MIN;

	/**
	 * The feature id for the '<em><b>Nb Cases Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_MOVING__NB_CASES_MAX = MOVE_GRID_ACTION__NB_CASES_MAX;

	/**
	 * The feature id for the '<em><b>Can Jump Other Entities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_MOVING__CAN_JUMP_OTHER_ENTITIES = MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES;

	/**
	 * The number of structural features of the '<em>Horizontal Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_MOVING_FEATURE_COUNT = MOVE_GRID_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Horizontal Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_MOVING_OPERATION_COUNT = MOVE_GRID_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.VerticalMovingImpl <em>Vertical Moving</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.VerticalMovingImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getVerticalMoving()
	 * @generated
	 */
	int VERTICAL_MOVING = 44;

	/**
	 * The feature id for the '<em><b>Nb Cases Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_MOVING__NB_CASES_MIN = MOVE_GRID_ACTION__NB_CASES_MIN;

	/**
	 * The feature id for the '<em><b>Nb Cases Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_MOVING__NB_CASES_MAX = MOVE_GRID_ACTION__NB_CASES_MAX;

	/**
	 * The feature id for the '<em><b>Can Jump Other Entities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_MOVING__CAN_JUMP_OTHER_ENTITIES = MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES;

	/**
	 * The number of structural features of the '<em>Vertical Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_MOVING_FEATURE_COUNT = MOVE_GRID_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Vertical Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_MOVING_OPERATION_COUNT = MOVE_GRID_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.DiagonalMovingImpl <em>Diagonal Moving</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.DiagonalMovingImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getDiagonalMoving()
	 * @generated
	 */
	int DIAGONAL_MOVING = 45;

	/**
	 * The feature id for the '<em><b>Nb Cases Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGONAL_MOVING__NB_CASES_MIN = MOVE_GRID_ACTION__NB_CASES_MIN;

	/**
	 * The feature id for the '<em><b>Nb Cases Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGONAL_MOVING__NB_CASES_MAX = MOVE_GRID_ACTION__NB_CASES_MAX;

	/**
	 * The feature id for the '<em><b>Can Jump Other Entities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGONAL_MOVING__CAN_JUMP_OTHER_ENTITIES = MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES;

	/**
	 * The number of structural features of the '<em>Diagonal Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGONAL_MOVING_FEATURE_COUNT = MOVE_GRID_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Diagonal Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIAGONAL_MOVING_OPERATION_COUNT = MOVE_GRID_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.CustomMovingImpl <em>Custom Moving</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.CustomMovingImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCustomMoving()
	 * @generated
	 */
	int CUSTOM_MOVING = 46;

	/**
	 * The feature id for the '<em><b>Nb Cases Min</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_MOVING__NB_CASES_MIN = MOVE_GRID_ACTION__NB_CASES_MIN;

	/**
	 * The feature id for the '<em><b>Nb Cases Max</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_MOVING__NB_CASES_MAX = MOVE_GRID_ACTION__NB_CASES_MAX;

	/**
	 * The feature id for the '<em><b>Can Jump Other Entities</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_MOVING__CAN_JUMP_OTHER_ENTITIES = MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES;

	/**
	 * The number of structural features of the '<em>Custom Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_MOVING_FEATURE_COUNT = MOVE_GRID_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Custom Moving</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUSTOM_MOVING_OPERATION_COUNT = MOVE_GRID_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.impl.FontImpl <em>Font</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.impl.FontImpl
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getFont()
	 * @generated
	 */
	int FONT = 47;

	/**
	 * The feature id for the '<em><b>Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FONT__PATH = RESOURCE__PATH;

	/**
	 * The number of structural features of the '<em>Font</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FONT_FEATURE_COUNT = RESOURCE_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Font</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FONT_OPERATION_COUNT = RESOURCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link LibGdxMM.PositionEnum <em>Position Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.PositionEnum
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPositionEnum()
	 * @generated
	 */
	int POSITION_ENUM = 48;

	/**
	 * The meta object id for the '{@link LibGdxMM.DragActionEnum <em>Drag Action Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.DragActionEnum
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getDragActionEnum()
	 * @generated
	 */
	int DRAG_ACTION_ENUM = 49;

	/**
	 * The meta object id for the '{@link LibGdxMM.ClickActionEnum <em>Click Action Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.ClickActionEnum
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getClickActionEnum()
	 * @generated
	 */
	int CLICK_ACTION_ENUM = 50;

	/**
	 * The meta object id for the '{@link LibGdxMM.ActActionEnum <em>Act Action Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.ActActionEnum
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getActActionEnum()
	 * @generated
	 */
	int ACT_ACTION_ENUM = 51;

	/**
	 * The meta object id for the '{@link LibGdxMM.XPosition <em>XPosition</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.XPosition
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getXPosition()
	 * @generated
	 */
	int XPOSITION = 52;

	/**
	 * The meta object id for the '{@link LibGdxMM.YPosition <em>YPosition</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.YPosition
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getYPosition()
	 * @generated
	 */
	int YPOSITION = 53;

	/**
	 * The meta object id for the '{@link LibGdxMM.BooleanEnum <em>Boolean Enum</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.BooleanEnum
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getBooleanEnum()
	 * @generated
	 */
	int BOOLEAN_ENUM = 54;

	/**
	 * The meta object id for the '{@link LibGdxMM.Color <em>Color</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see LibGdxMM.Color
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getColor()
	 * @generated
	 */
	int COLOR = 55;

	/**
	 * The meta object id for the '<em>String</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see String
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getString()
	 * @generated
	 */
	int STRING = 56;

	/**
	 * The meta object id for the '<em>Integer</em>' data type.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Integer
	 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getInteger()
	 * @generated
	 */
	int INTEGER = 57;


	/**
	 * Returns the meta object for class '{@link LibGdxMM.Game <em>Game</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Game</em>'.
	 * @see LibGdxMM.Game
	 * @generated
	 */
	EClass getGame();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Game#getScreens <em>Screens</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Screens</em>'.
	 * @see LibGdxMM.Game#getScreens()
	 * @see #getGame()
	 * @generated
	 */
	EReference getGame_Screens();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Game#getGamePackage <em>Game Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Game Package</em>'.
	 * @see LibGdxMM.Game#getGamePackage()
	 * @see #getGame()
	 * @generated
	 */
	EAttribute getGame_GamePackage();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Game#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see LibGdxMM.Game#getRoles()
	 * @see #getGame()
	 * @generated
	 */
	EReference getGame_Roles();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Game#getSdkLocation <em>Sdk Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Sdk Location</em>'.
	 * @see LibGdxMM.Game#getSdkLocation()
	 * @see #getGame()
	 * @generated
	 */
	EAttribute getGame_SdkLocation();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Game#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource</em>'.
	 * @see LibGdxMM.Game#getResource()
	 * @see #getGame()
	 * @generated
	 */
	EReference getGame_Resource();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see LibGdxMM.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see LibGdxMM.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Element</em>'.
	 * @see LibGdxMM.Element
	 * @generated
	 */
	EClass getElement();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Screen <em>Screen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Screen</em>'.
	 * @see LibGdxMM.Screen
	 * @generated
	 */
	EClass getScreen();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Screen#getActors <em>Actors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actors</em>'.
	 * @see LibGdxMM.Screen#getActors()
	 * @see #getScreen()
	 * @generated
	 */
	EReference getScreen_Actors();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.Screen#getWallpaper <em>Wallpaper</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Wallpaper</em>'.
	 * @see LibGdxMM.Screen#getWallpaper()
	 * @see #getScreen()
	 * @generated
	 */
	EReference getScreen_Wallpaper();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.Screen#getMusic <em>Music</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Music</em>'.
	 * @see LibGdxMM.Screen#getMusic()
	 * @see #getScreen()
	 * @generated
	 */
	EReference getScreen_Music();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Actor</em>'.
	 * @see LibGdxMM.Actor
	 * @generated
	 */
	EClass getActor();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Actor#getPosition <em>Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Position</em>'.
	 * @see LibGdxMM.Actor#getPosition()
	 * @see #getActor()
	 * @generated
	 */
	EAttribute getActor_Position();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Image <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Image</em>'.
	 * @see LibGdxMM.Image
	 * @generated
	 */
	EClass getImage();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Image#getRealWidth <em>Real Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Real Width</em>'.
	 * @see LibGdxMM.Image#getRealWidth()
	 * @see #getImage()
	 * @generated
	 */
	EAttribute getImage_RealWidth();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Image#getRealHeight <em>Real Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Real Height</em>'.
	 * @see LibGdxMM.Image#getRealHeight()
	 * @see #getImage()
	 * @generated
	 */
	EAttribute getImage_RealHeight();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see LibGdxMM.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Resource#getPath <em>Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Path</em>'.
	 * @see LibGdxMM.Resource#getPath()
	 * @see #getResource()
	 * @generated
	 */
	EAttribute getResource_Path();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Music <em>Music</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Music</em>'.
	 * @see LibGdxMM.Music
	 * @generated
	 */
	EClass getMusic();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see LibGdxMM.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.Role#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Image</em>'.
	 * @see LibGdxMM.Role#getImage()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_Image();

	/**
	 * Returns the meta object for the containment reference '{@link LibGdxMM.Role#getPixelAction <em>Pixel Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Pixel Action</em>'.
	 * @see LibGdxMM.Role#getPixelAction()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_PixelAction();

	/**
	 * Returns the meta object for the containment reference '{@link LibGdxMM.Role#getActAction <em>Act Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Act Action</em>'.
	 * @see LibGdxMM.Role#getActAction()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_ActAction();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Role#getCollision <em>Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Collision</em>'.
	 * @see LibGdxMM.Role#getCollision()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_Collision();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.PixelAction <em>Pixel Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pixel Action</em>'.
	 * @see LibGdxMM.PixelAction
	 * @generated
	 */
	EClass getPixelAction();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.PixelAction#getDrag <em>Drag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Drag</em>'.
	 * @see LibGdxMM.PixelAction#getDrag()
	 * @see #getPixelAction()
	 * @generated
	 */
	EAttribute getPixelAction_Drag();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.PixelAction#getClick <em>Click</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Click</em>'.
	 * @see LibGdxMM.PixelAction#getClick()
	 * @see #getPixelAction()
	 * @generated
	 */
	EAttribute getPixelAction_Click();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action</em>'.
	 * @see LibGdxMM.Action
	 * @generated
	 */
	EClass getAction();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.ActAction <em>Act Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Act Action</em>'.
	 * @see LibGdxMM.ActAction
	 * @generated
	 */
	EClass getActAction();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.ActAction#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action</em>'.
	 * @see LibGdxMM.ActAction#getAction()
	 * @see #getActAction()
	 * @generated
	 */
	EAttribute getActAction_Action();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Collision <em>Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collision</em>'.
	 * @see LibGdxMM.Collision
	 * @generated
	 */
	EClass getCollision();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.Collision#getWith <em>With</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>With</em>'.
	 * @see LibGdxMM.Collision#getWith()
	 * @see #getCollision()
	 * @generated
	 */
	EReference getCollision_With();

	/**
	 * Returns the meta object for the containment reference '{@link LibGdxMM.Collision#getCollisionType <em>Collision Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Collision Type</em>'.
	 * @see LibGdxMM.Collision#getCollisionType()
	 * @see #getCollision()
	 * @generated
	 */
	EReference getCollision_CollisionType();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.CollisionType <em>Collision Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Collision Type</em>'.
	 * @see LibGdxMM.CollisionType
	 * @generated
	 */
	EClass getCollisionType();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.CustomActor <em>Custom Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Actor</em>'.
	 * @see LibGdxMM.CustomActor
	 * @generated
	 */
	EClass getCustomActor();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.GameActor <em>Game Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Game Actor</em>'.
	 * @see LibGdxMM.GameActor
	 * @generated
	 */
	EClass getGameActor();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.GameActor#getRoleFactory <em>Role Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Factory</em>'.
	 * @see LibGdxMM.GameActor#getRoleFactory()
	 * @see #getGameActor()
	 * @generated
	 */
	EReference getGameActor_RoleFactory();

	/**
	 * Returns the meta object for the containment reference '{@link LibGdxMM.GameActor#getEngine <em>Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Engine</em>'.
	 * @see LibGdxMM.GameActor#getEngine()
	 * @see #getGameActor()
	 * @generated
	 */
	EReference getGameActor_Engine();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.RoleFactory <em>Role Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Factory</em>'.
	 * @see LibGdxMM.RoleFactory
	 * @generated
	 */
	EClass getRoleFactory();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.RoleFactory#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see LibGdxMM.RoleFactory#getRole()
	 * @see #getRoleFactory()
	 * @generated
	 */
	EReference getRoleFactory_Role();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.RoleFactory#getTimeToCreate <em>Time To Create</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time To Create</em>'.
	 * @see LibGdxMM.RoleFactory#getTimeToCreate()
	 * @see #getRoleFactory()
	 * @generated
	 */
	EAttribute getRoleFactory_TimeToCreate();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.RoleFactory#getSlackTime <em>Slack Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Slack Time</em>'.
	 * @see LibGdxMM.RoleFactory#getSlackTime()
	 * @see #getRoleFactory()
	 * @generated
	 */
	EAttribute getRoleFactory_SlackTime();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.RoleFactory#getXCreatedPosition <em>XCreated Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>XCreated Position</em>'.
	 * @see LibGdxMM.RoleFactory#getXCreatedPosition()
	 * @see #getRoleFactory()
	 * @generated
	 */
	EAttribute getRoleFactory_XCreatedPosition();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.RoleFactory#getYCreatedPosition <em>YCreated Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>YCreated Position</em>'.
	 * @see LibGdxMM.RoleFactory#getYCreatedPosition()
	 * @see #getRoleFactory()
	 * @generated
	 */
	EAttribute getRoleFactory_YCreatedPosition();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Engine <em>Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Engine</em>'.
	 * @see LibGdxMM.Engine
	 * @generated
	 */
	EClass getEngine();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.StatusBar <em>Status Bar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Status Bar</em>'.
	 * @see LibGdxMM.StatusBar
	 * @generated
	 */
	EClass getStatusBar();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Widget <em>Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Widget</em>'.
	 * @see LibGdxMM.Widget
	 * @generated
	 */
	EClass getWidget();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Button <em>Button</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Button</em>'.
	 * @see LibGdxMM.Button
	 * @generated
	 */
	EClass getButton();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.TextualWidget <em>Textual Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Textual Widget</em>'.
	 * @see LibGdxMM.TextualWidget
	 * @generated
	 */
	EClass getTextualWidget();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.TextualWidget#getText <em>Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text</em>'.
	 * @see LibGdxMM.TextualWidget#getText()
	 * @see #getTextualWidget()
	 * @generated
	 */
	EAttribute getTextualWidget_Text();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Label</em>'.
	 * @see LibGdxMM.Label
	 * @generated
	 */
	EClass getLabel();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.ImageWidget <em>Image Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Image Widget</em>'.
	 * @see LibGdxMM.ImageWidget
	 * @generated
	 */
	EClass getImageWidget();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.ImageWidget#getImage <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Image</em>'.
	 * @see LibGdxMM.ImageWidget#getImage()
	 * @see #getImageWidget()
	 * @generated
	 */
	EReference getImageWidget_Image();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.NextScreenButton <em>Next Screen Button</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Next Screen Button</em>'.
	 * @see LibGdxMM.NextScreenButton
	 * @generated
	 */
	EClass getNextScreenButton();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.NextScreenButton#getNextScreen <em>Next Screen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Next Screen</em>'.
	 * @see LibGdxMM.NextScreenButton#getNextScreen()
	 * @see #getNextScreenButton()
	 * @generated
	 */
	EReference getNextScreenButton_NextScreen();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.EnnemyInstance <em>Ennemy Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Ennemy Instance</em>'.
	 * @see LibGdxMM.EnnemyInstance
	 * @generated
	 */
	EClass getEnnemyInstance();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.RealTimeEngine <em>Real Time Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Real Time Engine</em>'.
	 * @see LibGdxMM.RealTimeEngine
	 * @generated
	 */
	EClass getRealTimeEngine();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.TurnBasedEngine <em>Turn Based Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Turn Based Engine</em>'.
	 * @see LibGdxMM.TurnBasedEngine
	 * @generated
	 */
	EClass getTurnBasedEngine();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Grid2DActor <em>Grid2 DActor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid2 DActor</em>'.
	 * @see LibGdxMM.Grid2DActor
	 * @generated
	 */
	EClass getGrid2DActor();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Grid2DActor#getGridEntities <em>Grid Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Grid Entities</em>'.
	 * @see LibGdxMM.Grid2DActor#getGridEntities()
	 * @see #getGrid2DActor()
	 * @generated
	 */
	EReference getGrid2DActor_GridEntities();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Grid2DActor#getGridWidth <em>Grid Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grid Width</em>'.
	 * @see LibGdxMM.Grid2DActor#getGridWidth()
	 * @see #getGrid2DActor()
	 * @generated
	 */
	EAttribute getGrid2DActor_GridWidth();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Grid2DActor#getGridHeight <em>Grid Height</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grid Height</em>'.
	 * @see LibGdxMM.Grid2DActor#getGridHeight()
	 * @see #getGrid2DActor()
	 * @generated
	 */
	EAttribute getGrid2DActor_GridHeight();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.Grid2DActor#getGridColor <em>Grid Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grid Color</em>'.
	 * @see LibGdxMM.Grid2DActor#getGridColor()
	 * @see #getGrid2DActor()
	 * @generated
	 */
	EAttribute getGrid2DActor_GridColor();

	/**
	 * Returns the meta object for the containment reference '{@link LibGdxMM.Grid2DActor#getAddGridRole <em>Add Grid Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Add Grid Role</em>'.
	 * @see LibGdxMM.Grid2DActor#getAddGridRole()
	 * @see #getGrid2DActor()
	 * @generated
	 */
	EReference getGrid2DActor_AddGridRole();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.PixelActor <em>Pixel Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pixel Actor</em>'.
	 * @see LibGdxMM.PixelActor
	 * @generated
	 */
	EClass getPixelActor();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.PixelActor#getPixelEntities <em>Pixel Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Pixel Entities</em>'.
	 * @see LibGdxMM.PixelActor#getPixelEntities()
	 * @see #getPixelActor()
	 * @generated
	 */
	EReference getPixelActor_PixelEntities();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.PixelEntity <em>Pixel Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pixel Entity</em>'.
	 * @see LibGdxMM.PixelEntity
	 * @generated
	 */
	EClass getPixelEntity();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.PixelEntity#getXPosition <em>XPosition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>XPosition</em>'.
	 * @see LibGdxMM.PixelEntity#getXPosition()
	 * @see #getPixelEntity()
	 * @generated
	 */
	EAttribute getPixelEntity_XPosition();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.PixelEntity#getYPosition <em>YPosition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>YPosition</em>'.
	 * @see LibGdxMM.PixelEntity#getYPosition()
	 * @see #getPixelEntity()
	 * @generated
	 */
	EAttribute getPixelEntity_YPosition();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.PixelEntity#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see LibGdxMM.PixelEntity#getRole()
	 * @see #getPixelEntity()
	 * @generated
	 */
	EReference getPixelEntity_Role();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see LibGdxMM.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.PixelRole <em>Pixel Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Pixel Role</em>'.
	 * @see LibGdxMM.PixelRole
	 * @generated
	 */
	EClass getPixelRole();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.GridEntity <em>Grid Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid Entity</em>'.
	 * @see LibGdxMM.GridEntity
	 * @generated
	 */
	EClass getGridEntity();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.GridEntity#getAbsPosition <em>Abs Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abs Position</em>'.
	 * @see LibGdxMM.GridEntity#getAbsPosition()
	 * @see #getGridEntity()
	 * @generated
	 */
	EAttribute getGridEntity_AbsPosition();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.GridEntity#getOrdPosition <em>Ord Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ord Position</em>'.
	 * @see LibGdxMM.GridEntity#getOrdPosition()
	 * @see #getGridEntity()
	 * @generated
	 */
	EAttribute getGridEntity_OrdPosition();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.GridEntity#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see LibGdxMM.GridEntity#getRole()
	 * @see #getGridEntity()
	 * @generated
	 */
	EReference getGridEntity_Role();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.GridRole <em>Grid Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid Role</em>'.
	 * @see LibGdxMM.GridRole
	 * @generated
	 */
	EClass getGridRole();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.GridRole#getMoveGridActions <em>Move Grid Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Move Grid Actions</em>'.
	 * @see LibGdxMM.GridRole#getMoveGridActions()
	 * @see #getGridRole()
	 * @generated
	 */
	EReference getGridRole_MoveGridActions();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.MoveGridAction <em>Move Grid Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Move Grid Action</em>'.
	 * @see LibGdxMM.MoveGridAction
	 * @generated
	 */
	EClass getMoveGridAction();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.MoveGridAction#getNbCasesMin <em>Nb Cases Min</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Cases Min</em>'.
	 * @see LibGdxMM.MoveGridAction#getNbCasesMin()
	 * @see #getMoveGridAction()
	 * @generated
	 */
	EAttribute getMoveGridAction_NbCasesMin();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.MoveGridAction#getNbCasesMax <em>Nb Cases Max</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nb Cases Max</em>'.
	 * @see LibGdxMM.MoveGridAction#getNbCasesMax()
	 * @see #getMoveGridAction()
	 * @generated
	 */
	EAttribute getMoveGridAction_NbCasesMax();

	/**
	 * Returns the meta object for the attribute '{@link LibGdxMM.MoveGridAction#getCanJumpOtherEntities <em>Can Jump Other Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Can Jump Other Entities</em>'.
	 * @see LibGdxMM.MoveGridAction#getCanJumpOtherEntities()
	 * @see #getMoveGridAction()
	 * @generated
	 */
	EAttribute getMoveGridAction_CanJumpOtherEntities();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.GridAction <em>Grid Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Grid Action</em>'.
	 * @see LibGdxMM.GridAction
	 * @generated
	 */
	EClass getGridAction();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.AddGridRole <em>Add Grid Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Add Grid Role</em>'.
	 * @see LibGdxMM.AddGridRole
	 * @generated
	 */
	EClass getAddGridRole();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.AddGridRole#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see LibGdxMM.AddGridRole#getRole()
	 * @see #getAddGridRole()
	 * @generated
	 */
	EReference getAddGridRole_Role();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Table <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Table</em>'.
	 * @see LibGdxMM.Table
	 * @generated
	 */
	EClass getTable();

	/**
	 * Returns the meta object for the containment reference list '{@link LibGdxMM.Table#getWidgets <em>Widgets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Widgets</em>'.
	 * @see LibGdxMM.Table#getWidgets()
	 * @see #getTable()
	 * @generated
	 */
	EReference getTable_Widgets();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.DestroyCollision <em>Destroy Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Destroy Collision</em>'.
	 * @see LibGdxMM.DestroyCollision
	 * @generated
	 */
	EClass getDestroyCollision();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.ChangeScreenCollision <em>Change Screen Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Change Screen Collision</em>'.
	 * @see LibGdxMM.ChangeScreenCollision
	 * @generated
	 */
	EClass getChangeScreenCollision();

	/**
	 * Returns the meta object for the reference '{@link LibGdxMM.ChangeScreenCollision#getScreen <em>Screen</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Screen</em>'.
	 * @see LibGdxMM.ChangeScreenCollision#getScreen()
	 * @see #getChangeScreenCollision()
	 * @generated
	 */
	EReference getChangeScreenCollision_Screen();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.CustomCollision <em>Custom Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Collision</em>'.
	 * @see LibGdxMM.CustomCollision
	 * @generated
	 */
	EClass getCustomCollision();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.QuitButton <em>Quit Button</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quit Button</em>'.
	 * @see LibGdxMM.QuitButton
	 * @generated
	 */
	EClass getQuitButton();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.HorizontalMoving <em>Horizontal Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Moving</em>'.
	 * @see LibGdxMM.HorizontalMoving
	 * @generated
	 */
	EClass getHorizontalMoving();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.VerticalMoving <em>Vertical Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Moving</em>'.
	 * @see LibGdxMM.VerticalMoving
	 * @generated
	 */
	EClass getVerticalMoving();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.DiagonalMoving <em>Diagonal Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Diagonal Moving</em>'.
	 * @see LibGdxMM.DiagonalMoving
	 * @generated
	 */
	EClass getDiagonalMoving();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.CustomMoving <em>Custom Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Custom Moving</em>'.
	 * @see LibGdxMM.CustomMoving
	 * @generated
	 */
	EClass getCustomMoving();

	/**
	 * Returns the meta object for class '{@link LibGdxMM.Font <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Font</em>'.
	 * @see LibGdxMM.Font
	 * @generated
	 */
	EClass getFont();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.PositionEnum <em>Position Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Position Enum</em>'.
	 * @see LibGdxMM.PositionEnum
	 * @generated
	 */
	EEnum getPositionEnum();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.DragActionEnum <em>Drag Action Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Drag Action Enum</em>'.
	 * @see LibGdxMM.DragActionEnum
	 * @generated
	 */
	EEnum getDragActionEnum();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.ClickActionEnum <em>Click Action Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Click Action Enum</em>'.
	 * @see LibGdxMM.ClickActionEnum
	 * @generated
	 */
	EEnum getClickActionEnum();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.ActActionEnum <em>Act Action Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Act Action Enum</em>'.
	 * @see LibGdxMM.ActActionEnum
	 * @generated
	 */
	EEnum getActActionEnum();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.XPosition <em>XPosition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>XPosition</em>'.
	 * @see LibGdxMM.XPosition
	 * @generated
	 */
	EEnum getXPosition();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.YPosition <em>YPosition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>YPosition</em>'.
	 * @see LibGdxMM.YPosition
	 * @generated
	 */
	EEnum getYPosition();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.BooleanEnum <em>Boolean Enum</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Boolean Enum</em>'.
	 * @see LibGdxMM.BooleanEnum
	 * @generated
	 */
	EEnum getBooleanEnum();

	/**
	 * Returns the meta object for enum '{@link LibGdxMM.Color <em>Color</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Color</em>'.
	 * @see LibGdxMM.Color
	 * @generated
	 */
	EEnum getColor();

	/**
	 * Returns the meta object for data type '{@link String <em>String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>String</em>'.
	 * @see String
	 * @model instanceClass="java.lang.Object"
	 * @generated
	 */
	EDataType getString();

	/**
	 * Returns the meta object for data type '{@link Integer <em>Integer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for data type '<em>Integer</em>'.
	 * @see Integer
	 * @model instanceClass="java.lang.Object"
	 * @generated
	 */
	EDataType getInteger();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	LibGdxMMFactory getLibGdxMMFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.GameImpl <em>Game</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.GameImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGame()
		 * @generated
		 */
		EClass GAME = eINSTANCE.getGame();

		/**
		 * The meta object literal for the '<em><b>Screens</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME__SCREENS = eINSTANCE.getGame_Screens();

		/**
		 * The meta object literal for the '<em><b>Game Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAME__GAME_PACKAGE = eINSTANCE.getGame_GamePackage();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME__ROLES = eINSTANCE.getGame_Roles();

		/**
		 * The meta object literal for the '<em><b>Sdk Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GAME__SDK_LOCATION = eINSTANCE.getGame_SdkLocation();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME__RESOURCE = eINSTANCE.getGame_Resource();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.NamedElementImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ElementImpl <em>Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ElementImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getElement()
		 * @generated
		 */
		EClass ELEMENT = eINSTANCE.getElement();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ScreenImpl <em>Screen</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ScreenImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getScreen()
		 * @generated
		 */
		EClass SCREEN = eINSTANCE.getScreen();

		/**
		 * The meta object literal for the '<em><b>Actors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCREEN__ACTORS = eINSTANCE.getScreen_Actors();

		/**
		 * The meta object literal for the '<em><b>Wallpaper</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCREEN__WALLPAPER = eINSTANCE.getScreen_Wallpaper();

		/**
		 * The meta object literal for the '<em><b>Music</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCREEN__MUSIC = eINSTANCE.getScreen_Music();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ActorImpl <em>Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ActorImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getActor()
		 * @generated
		 */
		EClass ACTOR = eINSTANCE.getActor();

		/**
		 * The meta object literal for the '<em><b>Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTOR__POSITION = eINSTANCE.getActor_Position();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ImageImpl <em>Image</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ImageImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getImage()
		 * @generated
		 */
		EClass IMAGE = eINSTANCE.getImage();

		/**
		 * The meta object literal for the '<em><b>Real Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMAGE__REAL_WIDTH = eINSTANCE.getImage_RealWidth();

		/**
		 * The meta object literal for the '<em><b>Real Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMAGE__REAL_HEIGHT = eINSTANCE.getImage_RealHeight();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ResourceImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE__PATH = eINSTANCE.getResource_Path();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.MusicImpl <em>Music</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.MusicImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getMusic()
		 * @generated
		 */
		EClass MUSIC = eINSTANCE.getMusic();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.RoleImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__IMAGE = eINSTANCE.getRole_Image();

		/**
		 * The meta object literal for the '<em><b>Pixel Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__PIXEL_ACTION = eINSTANCE.getRole_PixelAction();

		/**
		 * The meta object literal for the '<em><b>Act Action</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__ACT_ACTION = eINSTANCE.getRole_ActAction();

		/**
		 * The meta object literal for the '<em><b>Collision</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__COLLISION = eINSTANCE.getRole_Collision();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.PixelActionImpl <em>Pixel Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.PixelActionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelAction()
		 * @generated
		 */
		EClass PIXEL_ACTION = eINSTANCE.getPixelAction();

		/**
		 * The meta object literal for the '<em><b>Drag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIXEL_ACTION__DRAG = eINSTANCE.getPixelAction_Drag();

		/**
		 * The meta object literal for the '<em><b>Click</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIXEL_ACTION__CLICK = eINSTANCE.getPixelAction_Click();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ActionImpl <em>Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ActionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getAction()
		 * @generated
		 */
		EClass ACTION = eINSTANCE.getAction();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ActActionImpl <em>Act Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ActActionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getActAction()
		 * @generated
		 */
		EClass ACT_ACTION = eINSTANCE.getActAction();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACT_ACTION__ACTION = eINSTANCE.getActAction_Action();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.CollisionImpl <em>Collision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.CollisionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCollision()
		 * @generated
		 */
		EClass COLLISION = eINSTANCE.getCollision();

		/**
		 * The meta object literal for the '<em><b>With</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLISION__WITH = eINSTANCE.getCollision_With();

		/**
		 * The meta object literal for the '<em><b>Collision Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COLLISION__COLLISION_TYPE = eINSTANCE.getCollision_CollisionType();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.CollisionTypeImpl <em>Collision Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.CollisionTypeImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCollisionType()
		 * @generated
		 */
		EClass COLLISION_TYPE = eINSTANCE.getCollisionType();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.CustomActorImpl <em>Custom Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.CustomActorImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCustomActor()
		 * @generated
		 */
		EClass CUSTOM_ACTOR = eINSTANCE.getCustomActor();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.GameActorImpl <em>Game Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.GameActorImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGameActor()
		 * @generated
		 */
		EClass GAME_ACTOR = eINSTANCE.getGameActor();

		/**
		 * The meta object literal for the '<em><b>Role Factory</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME_ACTOR__ROLE_FACTORY = eINSTANCE.getGameActor_RoleFactory();

		/**
		 * The meta object literal for the '<em><b>Engine</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GAME_ACTOR__ENGINE = eINSTANCE.getGameActor_Engine();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.RoleFactoryImpl <em>Role Factory</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.RoleFactoryImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getRoleFactory()
		 * @generated
		 */
		EClass ROLE_FACTORY = eINSTANCE.getRoleFactory();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_FACTORY__ROLE = eINSTANCE.getRoleFactory_Role();

		/**
		 * The meta object literal for the '<em><b>Time To Create</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_FACTORY__TIME_TO_CREATE = eINSTANCE.getRoleFactory_TimeToCreate();

		/**
		 * The meta object literal for the '<em><b>Slack Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_FACTORY__SLACK_TIME = eINSTANCE.getRoleFactory_SlackTime();

		/**
		 * The meta object literal for the '<em><b>XCreated Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_FACTORY__XCREATED_POSITION = eINSTANCE.getRoleFactory_XCreatedPosition();

		/**
		 * The meta object literal for the '<em><b>YCreated Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_FACTORY__YCREATED_POSITION = eINSTANCE.getRoleFactory_YCreatedPosition();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.EngineImpl <em>Engine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.EngineImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getEngine()
		 * @generated
		 */
		EClass ENGINE = eINSTANCE.getEngine();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.StatusBarImpl <em>Status Bar</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.StatusBarImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getStatusBar()
		 * @generated
		 */
		EClass STATUS_BAR = eINSTANCE.getStatusBar();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.WidgetImpl <em>Widget</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.WidgetImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getWidget()
		 * @generated
		 */
		EClass WIDGET = eINSTANCE.getWidget();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ButtonImpl <em>Button</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ButtonImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getButton()
		 * @generated
		 */
		EClass BUTTON = eINSTANCE.getButton();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.TextualWidgetImpl <em>Textual Widget</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.TextualWidgetImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getTextualWidget()
		 * @generated
		 */
		EClass TEXTUAL_WIDGET = eINSTANCE.getTextualWidget();

		/**
		 * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEXTUAL_WIDGET__TEXT = eINSTANCE.getTextualWidget_Text();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.LabelImpl <em>Label</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.LabelImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getLabel()
		 * @generated
		 */
		EClass LABEL = eINSTANCE.getLabel();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ImageWidgetImpl <em>Image Widget</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ImageWidgetImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getImageWidget()
		 * @generated
		 */
		EClass IMAGE_WIDGET = eINSTANCE.getImageWidget();

		/**
		 * The meta object literal for the '<em><b>Image</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IMAGE_WIDGET__IMAGE = eINSTANCE.getImageWidget_Image();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.NextScreenButtonImpl <em>Next Screen Button</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.NextScreenButtonImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getNextScreenButton()
		 * @generated
		 */
		EClass NEXT_SCREEN_BUTTON = eINSTANCE.getNextScreenButton();

		/**
		 * The meta object literal for the '<em><b>Next Screen</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEXT_SCREEN_BUTTON__NEXT_SCREEN = eINSTANCE.getNextScreenButton_NextScreen();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.EnnemyInstanceImpl <em>Ennemy Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.EnnemyInstanceImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getEnnemyInstance()
		 * @generated
		 */
		EClass ENNEMY_INSTANCE = eINSTANCE.getEnnemyInstance();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.RealTimeEngineImpl <em>Real Time Engine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.RealTimeEngineImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getRealTimeEngine()
		 * @generated
		 */
		EClass REAL_TIME_ENGINE = eINSTANCE.getRealTimeEngine();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.TurnBasedEngineImpl <em>Turn Based Engine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.TurnBasedEngineImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getTurnBasedEngine()
		 * @generated
		 */
		EClass TURN_BASED_ENGINE = eINSTANCE.getTurnBasedEngine();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.Grid2DActorImpl <em>Grid2 DActor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.Grid2DActorImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGrid2DActor()
		 * @generated
		 */
		EClass GRID2_DACTOR = eINSTANCE.getGrid2DActor();

		/**
		 * The meta object literal for the '<em><b>Grid Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRID2_DACTOR__GRID_ENTITIES = eINSTANCE.getGrid2DActor_GridEntities();

		/**
		 * The meta object literal for the '<em><b>Grid Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID2_DACTOR__GRID_WIDTH = eINSTANCE.getGrid2DActor_GridWidth();

		/**
		 * The meta object literal for the '<em><b>Grid Height</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID2_DACTOR__GRID_HEIGHT = eINSTANCE.getGrid2DActor_GridHeight();

		/**
		 * The meta object literal for the '<em><b>Grid Color</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID2_DACTOR__GRID_COLOR = eINSTANCE.getGrid2DActor_GridColor();

		/**
		 * The meta object literal for the '<em><b>Add Grid Role</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRID2_DACTOR__ADD_GRID_ROLE = eINSTANCE.getGrid2DActor_AddGridRole();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.PixelActorImpl <em>Pixel Actor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.PixelActorImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelActor()
		 * @generated
		 */
		EClass PIXEL_ACTOR = eINSTANCE.getPixelActor();

		/**
		 * The meta object literal for the '<em><b>Pixel Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIXEL_ACTOR__PIXEL_ENTITIES = eINSTANCE.getPixelActor_PixelEntities();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.PixelEntityImpl <em>Pixel Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.PixelEntityImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelEntity()
		 * @generated
		 */
		EClass PIXEL_ENTITY = eINSTANCE.getPixelEntity();

		/**
		 * The meta object literal for the '<em><b>XPosition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIXEL_ENTITY__XPOSITION = eINSTANCE.getPixelEntity_XPosition();

		/**
		 * The meta object literal for the '<em><b>YPosition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PIXEL_ENTITY__YPOSITION = eINSTANCE.getPixelEntity_YPosition();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PIXEL_ENTITY__ROLE = eINSTANCE.getPixelEntity_Role();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.EntityImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.PixelRoleImpl <em>Pixel Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.PixelRoleImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPixelRole()
		 * @generated
		 */
		EClass PIXEL_ROLE = eINSTANCE.getPixelRole();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.GridEntityImpl <em>Grid Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.GridEntityImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGridEntity()
		 * @generated
		 */
		EClass GRID_ENTITY = eINSTANCE.getGridEntity();

		/**
		 * The meta object literal for the '<em><b>Abs Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_ENTITY__ABS_POSITION = eINSTANCE.getGridEntity_AbsPosition();

		/**
		 * The meta object literal for the '<em><b>Ord Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GRID_ENTITY__ORD_POSITION = eINSTANCE.getGridEntity_OrdPosition();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRID_ENTITY__ROLE = eINSTANCE.getGridEntity_Role();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.GridRoleImpl <em>Grid Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.GridRoleImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGridRole()
		 * @generated
		 */
		EClass GRID_ROLE = eINSTANCE.getGridRole();

		/**
		 * The meta object literal for the '<em><b>Move Grid Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference GRID_ROLE__MOVE_GRID_ACTIONS = eINSTANCE.getGridRole_MoveGridActions();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.MoveGridActionImpl <em>Move Grid Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.MoveGridActionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getMoveGridAction()
		 * @generated
		 */
		EClass MOVE_GRID_ACTION = eINSTANCE.getMoveGridAction();

		/**
		 * The meta object literal for the '<em><b>Nb Cases Min</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOVE_GRID_ACTION__NB_CASES_MIN = eINSTANCE.getMoveGridAction_NbCasesMin();

		/**
		 * The meta object literal for the '<em><b>Nb Cases Max</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOVE_GRID_ACTION__NB_CASES_MAX = eINSTANCE.getMoveGridAction_NbCasesMax();

		/**
		 * The meta object literal for the '<em><b>Can Jump Other Entities</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES = eINSTANCE.getMoveGridAction_CanJumpOtherEntities();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.GridActionImpl <em>Grid Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.GridActionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getGridAction()
		 * @generated
		 */
		EClass GRID_ACTION = eINSTANCE.getGridAction();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.AddGridRoleImpl <em>Add Grid Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.AddGridRoleImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getAddGridRole()
		 * @generated
		 */
		EClass ADD_GRID_ROLE = eINSTANCE.getAddGridRole();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADD_GRID_ROLE__ROLE = eINSTANCE.getAddGridRole_Role();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.TableImpl <em>Table</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.TableImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getTable()
		 * @generated
		 */
		EClass TABLE = eINSTANCE.getTable();

		/**
		 * The meta object literal for the '<em><b>Widgets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TABLE__WIDGETS = eINSTANCE.getTable_Widgets();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.DestroyCollisionImpl <em>Destroy Collision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.DestroyCollisionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getDestroyCollision()
		 * @generated
		 */
		EClass DESTROY_COLLISION = eINSTANCE.getDestroyCollision();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.ChangeScreenCollisionImpl <em>Change Screen Collision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.ChangeScreenCollisionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getChangeScreenCollision()
		 * @generated
		 */
		EClass CHANGE_SCREEN_COLLISION = eINSTANCE.getChangeScreenCollision();

		/**
		 * The meta object literal for the '<em><b>Screen</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CHANGE_SCREEN_COLLISION__SCREEN = eINSTANCE.getChangeScreenCollision_Screen();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.CustomCollisionImpl <em>Custom Collision</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.CustomCollisionImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCustomCollision()
		 * @generated
		 */
		EClass CUSTOM_COLLISION = eINSTANCE.getCustomCollision();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.QuitButtonImpl <em>Quit Button</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.QuitButtonImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getQuitButton()
		 * @generated
		 */
		EClass QUIT_BUTTON = eINSTANCE.getQuitButton();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.HorizontalMovingImpl <em>Horizontal Moving</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.HorizontalMovingImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getHorizontalMoving()
		 * @generated
		 */
		EClass HORIZONTAL_MOVING = eINSTANCE.getHorizontalMoving();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.VerticalMovingImpl <em>Vertical Moving</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.VerticalMovingImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getVerticalMoving()
		 * @generated
		 */
		EClass VERTICAL_MOVING = eINSTANCE.getVerticalMoving();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.DiagonalMovingImpl <em>Diagonal Moving</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.DiagonalMovingImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getDiagonalMoving()
		 * @generated
		 */
		EClass DIAGONAL_MOVING = eINSTANCE.getDiagonalMoving();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.CustomMovingImpl <em>Custom Moving</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.CustomMovingImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getCustomMoving()
		 * @generated
		 */
		EClass CUSTOM_MOVING = eINSTANCE.getCustomMoving();

		/**
		 * The meta object literal for the '{@link LibGdxMM.impl.FontImpl <em>Font</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.impl.FontImpl
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getFont()
		 * @generated
		 */
		EClass FONT = eINSTANCE.getFont();

		/**
		 * The meta object literal for the '{@link LibGdxMM.PositionEnum <em>Position Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.PositionEnum
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getPositionEnum()
		 * @generated
		 */
		EEnum POSITION_ENUM = eINSTANCE.getPositionEnum();

		/**
		 * The meta object literal for the '{@link LibGdxMM.DragActionEnum <em>Drag Action Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.DragActionEnum
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getDragActionEnum()
		 * @generated
		 */
		EEnum DRAG_ACTION_ENUM = eINSTANCE.getDragActionEnum();

		/**
		 * The meta object literal for the '{@link LibGdxMM.ClickActionEnum <em>Click Action Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.ClickActionEnum
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getClickActionEnum()
		 * @generated
		 */
		EEnum CLICK_ACTION_ENUM = eINSTANCE.getClickActionEnum();

		/**
		 * The meta object literal for the '{@link LibGdxMM.ActActionEnum <em>Act Action Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.ActActionEnum
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getActActionEnum()
		 * @generated
		 */
		EEnum ACT_ACTION_ENUM = eINSTANCE.getActActionEnum();

		/**
		 * The meta object literal for the '{@link LibGdxMM.XPosition <em>XPosition</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.XPosition
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getXPosition()
		 * @generated
		 */
		EEnum XPOSITION = eINSTANCE.getXPosition();

		/**
		 * The meta object literal for the '{@link LibGdxMM.YPosition <em>YPosition</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.YPosition
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getYPosition()
		 * @generated
		 */
		EEnum YPOSITION = eINSTANCE.getYPosition();

		/**
		 * The meta object literal for the '{@link LibGdxMM.BooleanEnum <em>Boolean Enum</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.BooleanEnum
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getBooleanEnum()
		 * @generated
		 */
		EEnum BOOLEAN_ENUM = eINSTANCE.getBooleanEnum();

		/**
		 * The meta object literal for the '{@link LibGdxMM.Color <em>Color</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see LibGdxMM.Color
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getColor()
		 * @generated
		 */
		EEnum COLOR = eINSTANCE.getColor();

		/**
		 * The meta object literal for the '<em>String</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see String
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getString()
		 * @generated
		 */
		EDataType STRING = eINSTANCE.getString();

		/**
		 * The meta object literal for the '<em>Integer</em>' data type.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Integer
		 * @see LibGdxMM.impl.LibGdxMMPackageImpl#getInteger()
		 * @generated
		 */
		EDataType INTEGER = eINSTANCE.getInteger();

	}

} //LibGdxMMPackage
