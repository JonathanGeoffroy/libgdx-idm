/**
 */
package LibGdxMM;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Next Screen Button</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Bouton spécial permettant de changer de Screen au clic
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link LibGdxMM.NextScreenButton#getNextScreen <em>Next Screen</em>}</li>
 * </ul>
 * </p>
 *
 * @see LibGdxMM.LibGdxMMPackage#getNextScreenButton()
 * @model
 * @generated
 */
public interface NextScreenButton extends Button {
	/**
	 * Returns the value of the '<em><b>Next Screen</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Screen</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Screen</em>' reference.
	 * @see #setNextScreen(Screen)
	 * @see LibGdxMM.LibGdxMMPackage#getNextScreenButton_NextScreen()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Screen getNextScreen();

	/**
	 * Sets the value of the '{@link LibGdxMM.NextScreenButton#getNextScreen <em>Next Screen</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Next Screen</em>' reference.
	 * @see #getNextScreen()
	 * @generated
	 */
	void setNextScreen(Screen value);

} // NextScreenButton
