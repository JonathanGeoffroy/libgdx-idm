/**
 */
package LibGdxMM;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Screen</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * Une projet est un découpage en Screen.
 * Chaque screen représente ce que l'utilisateur voit sur un seul écran.
 * Typiquement, tout l'écran est chargé à chaque fois qu'on change d'écran.
 * Similaire aux Activity Android
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link LibGdxMM.Screen#getActors <em>Actors</em>}</li>
 *   <li>{@link LibGdxMM.Screen#getWallpaper <em>Wallpaper</em>}</li>
 *   <li>{@link LibGdxMM.Screen#getMusic <em>Music</em>}</li>
 * </ul>
 * </p>
 *
 * @see LibGdxMM.LibGdxMMPackage#getScreen()
 * @model
 * @generated
 */
public interface Screen extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Actors</b></em>' containment reference list.
	 * The list contents are of type {@link LibGdxMM.Actor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actors</em>' containment reference list.
	 * @see LibGdxMM.LibGdxMMPackage#getScreen_Actors()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Actor> getActors();

	/**
	 * Returns the value of the '<em><b>Wallpaper</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wallpaper</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wallpaper</em>' reference.
	 * @see #setWallpaper(Image)
	 * @see LibGdxMM.LibGdxMMPackage#getScreen_Wallpaper()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Image getWallpaper();

	/**
	 * Sets the value of the '{@link LibGdxMM.Screen#getWallpaper <em>Wallpaper</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Wallpaper</em>' reference.
	 * @see #getWallpaper()
	 * @generated
	 */
	void setWallpaper(Image value);

	/**
	 * Returns the value of the '<em><b>Music</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Music</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Music</em>' reference.
	 * @see #setMusic(Music)
	 * @see LibGdxMM.LibGdxMMPackage#getScreen_Music()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Music getMusic();

	/**
	 * Sets the value of the '{@link LibGdxMM.Screen#getMusic <em>Music</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Music</em>' reference.
	 * @see #getMusic()
	 * @generated
	 */
	void setMusic(Music value);

} // Screen
