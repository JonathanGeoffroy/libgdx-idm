/**
 */
package LibGdxMM.impl;

import LibGdxMM.Game;
import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.Resource;
import LibGdxMM.Role;
import LibGdxMM.Screen;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Game</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link LibGdxMM.impl.GameImpl#getScreens <em>Screens</em>}</li>
 *   <li>{@link LibGdxMM.impl.GameImpl#getGamePackage <em>Game Package</em>}</li>
 *   <li>{@link LibGdxMM.impl.GameImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link LibGdxMM.impl.GameImpl#getSdkLocation <em>Sdk Location</em>}</li>
 *   <li>{@link LibGdxMM.impl.GameImpl#getResource <em>Resource</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class GameImpl extends NamedElementImpl implements Game {
	/**
	 * The cached value of the '{@link #getScreens() <em>Screens</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScreens()
	 * @generated
	 * @ordered
	 */
	protected EList<Screen> screens;

	/**
	 * The default value of the '{@link #getGamePackage() <em>Game Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGamePackage()
	 * @generated
	 * @ordered
	 */
	protected static final Object GAME_PACKAGE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getGamePackage() <em>Game Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGamePackage()
	 * @generated
	 * @ordered
	 */
	protected Object gamePackage = GAME_PACKAGE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> roles;

	/**
	 * The default value of the '{@link #getSdkLocation() <em>Sdk Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdkLocation()
	 * @generated
	 * @ordered
	 */
	protected static final Object SDK_LOCATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSdkLocation() <em>Sdk Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSdkLocation()
	 * @generated
	 * @ordered
	 */
	protected Object sdkLocation = SDK_LOCATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected GameImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibGdxMMPackage.Literals.GAME;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Screen> getScreens() {
		if (screens == null) {
			screens = new EObjectContainmentEList<Screen>(Screen.class, this, LibGdxMMPackage.GAME__SCREENS);
		}
		return screens;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getGamePackage() {
		return gamePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGamePackage(Object newGamePackage) {
		Object oldGamePackage = gamePackage;
		gamePackage = newGamePackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibGdxMMPackage.GAME__GAME_PACKAGE, oldGamePackage, gamePackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getRoles() {
		if (roles == null) {
			roles = new EObjectContainmentEList<Role>(Role.class, this, LibGdxMMPackage.GAME__ROLES);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getSdkLocation() {
		return sdkLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSdkLocation(Object newSdkLocation) {
		Object oldSdkLocation = sdkLocation;
		sdkLocation = newSdkLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibGdxMMPackage.GAME__SDK_LOCATION, oldSdkLocation, sdkLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResource() {
		if (resource == null) {
			resource = new EObjectContainmentEList<Resource>(Resource.class, this, LibGdxMMPackage.GAME__RESOURCE);
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibGdxMMPackage.GAME__SCREENS:
				return ((InternalEList<?>)getScreens()).basicRemove(otherEnd, msgs);
			case LibGdxMMPackage.GAME__ROLES:
				return ((InternalEList<?>)getRoles()).basicRemove(otherEnd, msgs);
			case LibGdxMMPackage.GAME__RESOURCE:
				return ((InternalEList<?>)getResource()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibGdxMMPackage.GAME__SCREENS:
				return getScreens();
			case LibGdxMMPackage.GAME__GAME_PACKAGE:
				return getGamePackage();
			case LibGdxMMPackage.GAME__ROLES:
				return getRoles();
			case LibGdxMMPackage.GAME__SDK_LOCATION:
				return getSdkLocation();
			case LibGdxMMPackage.GAME__RESOURCE:
				return getResource();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibGdxMMPackage.GAME__SCREENS:
				getScreens().clear();
				getScreens().addAll((Collection<? extends Screen>)newValue);
				return;
			case LibGdxMMPackage.GAME__GAME_PACKAGE:
				setGamePackage((Object)newValue);
				return;
			case LibGdxMMPackage.GAME__ROLES:
				getRoles().clear();
				getRoles().addAll((Collection<? extends Role>)newValue);
				return;
			case LibGdxMMPackage.GAME__SDK_LOCATION:
				setSdkLocation((Object)newValue);
				return;
			case LibGdxMMPackage.GAME__RESOURCE:
				getResource().clear();
				getResource().addAll((Collection<? extends Resource>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibGdxMMPackage.GAME__SCREENS:
				getScreens().clear();
				return;
			case LibGdxMMPackage.GAME__GAME_PACKAGE:
				setGamePackage(GAME_PACKAGE_EDEFAULT);
				return;
			case LibGdxMMPackage.GAME__ROLES:
				getRoles().clear();
				return;
			case LibGdxMMPackage.GAME__SDK_LOCATION:
				setSdkLocation(SDK_LOCATION_EDEFAULT);
				return;
			case LibGdxMMPackage.GAME__RESOURCE:
				getResource().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibGdxMMPackage.GAME__SCREENS:
				return screens != null && !screens.isEmpty();
			case LibGdxMMPackage.GAME__GAME_PACKAGE:
				return GAME_PACKAGE_EDEFAULT == null ? gamePackage != null : !GAME_PACKAGE_EDEFAULT.equals(gamePackage);
			case LibGdxMMPackage.GAME__ROLES:
				return roles != null && !roles.isEmpty();
			case LibGdxMMPackage.GAME__SDK_LOCATION:
				return SDK_LOCATION_EDEFAULT == null ? sdkLocation != null : !SDK_LOCATION_EDEFAULT.equals(sdkLocation);
			case LibGdxMMPackage.GAME__RESOURCE:
				return resource != null && !resource.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (gamePackage: ");
		result.append(gamePackage);
		result.append(", sdkLocation: ");
		result.append(sdkLocation);
		result.append(')');
		return result.toString();
	}

} //GameImpl
