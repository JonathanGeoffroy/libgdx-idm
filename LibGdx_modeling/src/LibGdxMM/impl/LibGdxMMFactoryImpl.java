/**
 */
package LibGdxMM.impl;

import LibGdxMM.*;
import LibGdxMM.ActAction;
import LibGdxMM.ActActionEnum;
import LibGdxMM.AddGridRole;
import LibGdxMM.Button;
import LibGdxMM.ChangeScreenCollision;
import LibGdxMM.ClickActionEnum;
import LibGdxMM.Collision;
import LibGdxMM.Color;
import LibGdxMM.CustomActor;
import LibGdxMM.CustomCollision;
import LibGdxMM.CustomMoving;
import LibGdxMM.DestroyCollision;
import LibGdxMM.DiagonalMoving;
import LibGdxMM.DragActionEnum;
import LibGdxMM.EnnemyInstance;
import LibGdxMM.Font;
import LibGdxMM.Game;
import LibGdxMM.Grid2DActor;
import LibGdxMM.GridEntity;
import LibGdxMM.GridRole;
import LibGdxMM.HorizontalMoving;
import LibGdxMM.Image;
import LibGdxMM.ImageWidget;
import LibGdxMM.Label;
import LibGdxMM.LibGdxMMFactory;
import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.Music;
import LibGdxMM.NextScreenButton;
import LibGdxMM.PixelAction;
import LibGdxMM.PixelActor;
import LibGdxMM.PixelEntity;
import LibGdxMM.PixelRole;
import LibGdxMM.PositionEnum;
import LibGdxMM.QuitButton;
import LibGdxMM.RealTimeEngine;
import LibGdxMM.RoleFactory;
import LibGdxMM.Screen;
import LibGdxMM.StatusBar;
import LibGdxMM.Table;
import LibGdxMM.TurnBasedEngine;
import LibGdxMM.VerticalMoving;
import LibGdxMM.XPosition;
import LibGdxMM.YPosition;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LibGdxMMFactoryImpl extends EFactoryImpl implements LibGdxMMFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LibGdxMMFactory init() {
		try {
			LibGdxMMFactory theLibGdxMMFactory = (LibGdxMMFactory)EPackage.Registry.INSTANCE.getEFactory(LibGdxMMPackage.eNS_URI);
			if (theLibGdxMMFactory != null) {
				return theLibGdxMMFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new LibGdxMMFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibGdxMMFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case LibGdxMMPackage.GAME: return createGame();
			case LibGdxMMPackage.SCREEN: return createScreen();
			case LibGdxMMPackage.IMAGE: return createImage();
			case LibGdxMMPackage.MUSIC: return createMusic();
			case LibGdxMMPackage.PIXEL_ACTION: return createPixelAction();
			case LibGdxMMPackage.ACT_ACTION: return createActAction();
			case LibGdxMMPackage.COLLISION: return createCollision();
			case LibGdxMMPackage.CUSTOM_ACTOR: return createCustomActor();
			case LibGdxMMPackage.ROLE_FACTORY: return createRoleFactory();
			case LibGdxMMPackage.STATUS_BAR: return createStatusBar();
			case LibGdxMMPackage.BUTTON: return createButton();
			case LibGdxMMPackage.LABEL: return createLabel();
			case LibGdxMMPackage.IMAGE_WIDGET: return createImageWidget();
			case LibGdxMMPackage.NEXT_SCREEN_BUTTON: return createNextScreenButton();
			case LibGdxMMPackage.ENNEMY_INSTANCE: return createEnnemyInstance();
			case LibGdxMMPackage.REAL_TIME_ENGINE: return createRealTimeEngine();
			case LibGdxMMPackage.TURN_BASED_ENGINE: return createTurnBasedEngine();
			case LibGdxMMPackage.GRID2_DACTOR: return createGrid2DActor();
			case LibGdxMMPackage.PIXEL_ACTOR: return createPixelActor();
			case LibGdxMMPackage.PIXEL_ENTITY: return createPixelEntity();
			case LibGdxMMPackage.PIXEL_ROLE: return createPixelRole();
			case LibGdxMMPackage.GRID_ENTITY: return createGridEntity();
			case LibGdxMMPackage.GRID_ROLE: return createGridRole();
			case LibGdxMMPackage.ADD_GRID_ROLE: return createAddGridRole();
			case LibGdxMMPackage.TABLE: return createTable();
			case LibGdxMMPackage.DESTROY_COLLISION: return createDestroyCollision();
			case LibGdxMMPackage.CHANGE_SCREEN_COLLISION: return createChangeScreenCollision();
			case LibGdxMMPackage.CUSTOM_COLLISION: return createCustomCollision();
			case LibGdxMMPackage.QUIT_BUTTON: return createQuitButton();
			case LibGdxMMPackage.HORIZONTAL_MOVING: return createHorizontalMoving();
			case LibGdxMMPackage.VERTICAL_MOVING: return createVerticalMoving();
			case LibGdxMMPackage.DIAGONAL_MOVING: return createDiagonalMoving();
			case LibGdxMMPackage.CUSTOM_MOVING: return createCustomMoving();
			case LibGdxMMPackage.FONT: return createFont();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case LibGdxMMPackage.POSITION_ENUM:
				return createPositionEnumFromString(eDataType, initialValue);
			case LibGdxMMPackage.DRAG_ACTION_ENUM:
				return createDragActionEnumFromString(eDataType, initialValue);
			case LibGdxMMPackage.CLICK_ACTION_ENUM:
				return createClickActionEnumFromString(eDataType, initialValue);
			case LibGdxMMPackage.ACT_ACTION_ENUM:
				return createActActionEnumFromString(eDataType, initialValue);
			case LibGdxMMPackage.XPOSITION:
				return createXPositionFromString(eDataType, initialValue);
			case LibGdxMMPackage.YPOSITION:
				return createYPositionFromString(eDataType, initialValue);
			case LibGdxMMPackage.BOOLEAN_ENUM:
				return createBooleanEnumFromString(eDataType, initialValue);
			case LibGdxMMPackage.COLOR:
				return createColorFromString(eDataType, initialValue);
			case LibGdxMMPackage.STRING:
				return createStringFromString(eDataType, initialValue);
			case LibGdxMMPackage.INTEGER:
				return createIntegerFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case LibGdxMMPackage.POSITION_ENUM:
				return convertPositionEnumToString(eDataType, instanceValue);
			case LibGdxMMPackage.DRAG_ACTION_ENUM:
				return convertDragActionEnumToString(eDataType, instanceValue);
			case LibGdxMMPackage.CLICK_ACTION_ENUM:
				return convertClickActionEnumToString(eDataType, instanceValue);
			case LibGdxMMPackage.ACT_ACTION_ENUM:
				return convertActActionEnumToString(eDataType, instanceValue);
			case LibGdxMMPackage.XPOSITION:
				return convertXPositionToString(eDataType, instanceValue);
			case LibGdxMMPackage.YPOSITION:
				return convertYPositionToString(eDataType, instanceValue);
			case LibGdxMMPackage.BOOLEAN_ENUM:
				return convertBooleanEnumToString(eDataType, instanceValue);
			case LibGdxMMPackage.COLOR:
				return convertColorToString(eDataType, instanceValue);
			case LibGdxMMPackage.STRING:
				return convertStringToString(eDataType, instanceValue);
			case LibGdxMMPackage.INTEGER:
				return convertIntegerToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Game createGame() {
		GameImpl game = new GameImpl();
		return game;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen createScreen() {
		ScreenImpl screen = new ScreenImpl();
		return screen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Image createImage() {
		ImageImpl image = new ImageImpl();
		return image;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Music createMusic() {
		MusicImpl music = new MusicImpl();
		return music;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PixelAction createPixelAction() {
		PixelActionImpl pixelAction = new PixelActionImpl();
		return pixelAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActAction createActAction() {
		ActActionImpl actAction = new ActActionImpl();
		return actAction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Collision createCollision() {
		CollisionImpl collision = new CollisionImpl();
		return collision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomActor createCustomActor() {
		CustomActorImpl customActor = new CustomActorImpl();
		return customActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoleFactory createRoleFactory() {
		RoleFactoryImpl roleFactory = new RoleFactoryImpl();
		return roleFactory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusBar createStatusBar() {
		StatusBarImpl statusBar = new StatusBarImpl();
		return statusBar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Button createButton() {
		ButtonImpl button = new ButtonImpl();
		return button;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Label createLabel() {
		LabelImpl label = new LabelImpl();
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageWidget createImageWidget() {
		ImageWidgetImpl imageWidget = new ImageWidgetImpl();
		return imageWidget;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NextScreenButton createNextScreenButton() {
		NextScreenButtonImpl nextScreenButton = new NextScreenButtonImpl();
		return nextScreenButton;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnnemyInstance createEnnemyInstance() {
		EnnemyInstanceImpl ennemyInstance = new EnnemyInstanceImpl();
		return ennemyInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RealTimeEngine createRealTimeEngine() {
		RealTimeEngineImpl realTimeEngine = new RealTimeEngineImpl();
		return realTimeEngine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TurnBasedEngine createTurnBasedEngine() {
		TurnBasedEngineImpl turnBasedEngine = new TurnBasedEngineImpl();
		return turnBasedEngine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Grid2DActor createGrid2DActor() {
		Grid2DActorImpl grid2DActor = new Grid2DActorImpl();
		return grid2DActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PixelActor createPixelActor() {
		PixelActorImpl pixelActor = new PixelActorImpl();
		return pixelActor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PixelEntity createPixelEntity() {
		PixelEntityImpl pixelEntity = new PixelEntityImpl();
		return pixelEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PixelRole createPixelRole() {
		PixelRoleImpl pixelRole = new PixelRoleImpl();
		return pixelRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GridEntity createGridEntity() {
		GridEntityImpl gridEntity = new GridEntityImpl();
		return gridEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GridRole createGridRole() {
		GridRoleImpl gridRole = new GridRoleImpl();
		return gridRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AddGridRole createAddGridRole() {
		AddGridRoleImpl addGridRole = new AddGridRoleImpl();
		return addGridRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Table createTable() {
		TableImpl table = new TableImpl();
		return table;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DestroyCollision createDestroyCollision() {
		DestroyCollisionImpl destroyCollision = new DestroyCollisionImpl();
		return destroyCollision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChangeScreenCollision createChangeScreenCollision() {
		ChangeScreenCollisionImpl changeScreenCollision = new ChangeScreenCollisionImpl();
		return changeScreenCollision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomCollision createCustomCollision() {
		CustomCollisionImpl customCollision = new CustomCollisionImpl();
		return customCollision;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public QuitButton createQuitButton() {
		QuitButtonImpl quitButton = new QuitButtonImpl();
		return quitButton;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HorizontalMoving createHorizontalMoving() {
		HorizontalMovingImpl horizontalMoving = new HorizontalMovingImpl();
		return horizontalMoving;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VerticalMoving createVerticalMoving() {
		VerticalMovingImpl verticalMoving = new VerticalMovingImpl();
		return verticalMoving;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DiagonalMoving createDiagonalMoving() {
		DiagonalMovingImpl diagonalMoving = new DiagonalMovingImpl();
		return diagonalMoving;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CustomMoving createCustomMoving() {
		CustomMovingImpl customMoving = new CustomMovingImpl();
		return customMoving;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Font createFont() {
		FontImpl font = new FontImpl();
		return font;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PositionEnum createPositionEnumFromString(EDataType eDataType, String initialValue) {
		PositionEnum result = PositionEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPositionEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DragActionEnum createDragActionEnumFromString(EDataType eDataType, String initialValue) {
		DragActionEnum result = DragActionEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDragActionEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClickActionEnum createClickActionEnumFromString(EDataType eDataType, String initialValue) {
		ClickActionEnum result = ClickActionEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertClickActionEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActActionEnum createActActionEnumFromString(EDataType eDataType, String initialValue) {
		ActActionEnum result = ActActionEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertActActionEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XPosition createXPositionFromString(EDataType eDataType, String initialValue) {
		XPosition result = XPosition.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertXPositionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public YPosition createYPositionFromString(EDataType eDataType, String initialValue) {
		YPosition result = YPosition.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertYPositionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BooleanEnum createBooleanEnumFromString(EDataType eDataType, String initialValue) {
		BooleanEnum result = BooleanEnum.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertBooleanEnumToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Color createColorFromString(EDataType eDataType, String initialValue) {
		Color result = Color.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertColorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createStringFromString(EDataType eDataType, String initialValue) {
		return (Object)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStringToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object createIntegerFromString(EDataType eDataType, String initialValue) {
		return (Object)super.createFromString(eDataType, initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertIntegerToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibGdxMMPackage getLibGdxMMPackage() {
		return (LibGdxMMPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static LibGdxMMPackage getPackage() {
		return LibGdxMMPackage.eINSTANCE;
	}

} //LibGdxMMFactoryImpl
