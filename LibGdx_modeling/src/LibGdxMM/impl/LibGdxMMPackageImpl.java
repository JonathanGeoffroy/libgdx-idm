/**
 */
package LibGdxMM.impl;

import LibGdxMM.ActAction;
import LibGdxMM.ActActionEnum;
import LibGdxMM.Action;
import LibGdxMM.Actor;
import LibGdxMM.AddGridRole;
import LibGdxMM.BooleanEnum;
import LibGdxMM.Button;
import LibGdxMM.ChangeScreenCollision;
import LibGdxMM.ClickActionEnum;
import LibGdxMM.Collision;
import LibGdxMM.CollisionType;
import LibGdxMM.Color;
import LibGdxMM.CustomActor;
import LibGdxMM.CustomCollision;
import LibGdxMM.CustomMoving;
import LibGdxMM.DestroyCollision;
import LibGdxMM.DiagonalMoving;
import LibGdxMM.DragActionEnum;
import LibGdxMM.Element;
import LibGdxMM.Engine;
import LibGdxMM.EnnemyInstance;
import LibGdxMM.Entity;
import LibGdxMM.Font;
import LibGdxMM.Game;
import LibGdxMM.GameActor;
import LibGdxMM.Grid2DActor;
import LibGdxMM.GridAction;
import LibGdxMM.GridEntity;
import LibGdxMM.GridRole;
import LibGdxMM.HorizontalMoving;
import LibGdxMM.Image;
import LibGdxMM.ImageWidget;
import LibGdxMM.Label;
import LibGdxMM.LibGdxMMFactory;
import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.MoveGridAction;
import LibGdxMM.Music;
import LibGdxMM.NamedElement;
import LibGdxMM.NextScreenButton;
import LibGdxMM.PixelAction;
import LibGdxMM.PixelActor;
import LibGdxMM.PixelEntity;
import LibGdxMM.PixelRole;
import LibGdxMM.PositionEnum;
import LibGdxMM.QuitButton;
import LibGdxMM.RealTimeEngine;
import LibGdxMM.Resource;
import LibGdxMM.Role;
import LibGdxMM.RoleFactory;
import LibGdxMM.Screen;
import LibGdxMM.StatusBar;
import LibGdxMM.Table;
import LibGdxMM.TextualWidget;
import LibGdxMM.TurnBasedEngine;
import LibGdxMM.VerticalMoving;
import LibGdxMM.Widget;
import LibGdxMM.XPosition;
import LibGdxMM.YPosition;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class LibGdxMMPackageImpl extends EPackageImpl implements LibGdxMMPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gameEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass elementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass screenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass musicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pixelActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass collisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass collisionTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gameActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleFactoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass engineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass statusBarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass widgetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buttonEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass textualWidgetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass labelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass imageWidgetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nextScreenButtonEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ennemyInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass realTimeEngineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass turnBasedEngineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass grid2DActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pixelActorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pixelEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass entityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass pixelRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gridEntityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gridRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moveGridActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass gridActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass addGridRoleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass destroyCollisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass changeScreenCollisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customCollisionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass quitButtonEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontalMovingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass verticalMovingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass diagonalMovingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass customMovingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass fontEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum positionEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum dragActionEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum clickActionEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum actActionEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum xPositionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum yPositionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum booleanEnumEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum colorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType stringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType integerEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see LibGdxMM.LibGdxMMPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private LibGdxMMPackageImpl() {
		super(eNS_URI, LibGdxMMFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link LibGdxMMPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static LibGdxMMPackage init() {
		if (isInited) return (LibGdxMMPackage)EPackage.Registry.INSTANCE.getEPackage(LibGdxMMPackage.eNS_URI);

		// Obtain or create and register package
		LibGdxMMPackageImpl theLibGdxMMPackage = (LibGdxMMPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof LibGdxMMPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new LibGdxMMPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theLibGdxMMPackage.createPackageContents();

		// Initialize created meta-data
		theLibGdxMMPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theLibGdxMMPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(LibGdxMMPackage.eNS_URI, theLibGdxMMPackage);
		return theLibGdxMMPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGame() {
		return gameEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGame_Screens() {
		return (EReference)gameEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGame_GamePackage() {
		return (EAttribute)gameEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGame_Roles() {
		return (EReference)gameEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGame_SdkLocation() {
		return (EAttribute)gameEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGame_Resource() {
		return (EReference)gameEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getElement() {
		return elementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScreen() {
		return screenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScreen_Actors() {
		return (EReference)screenEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScreen_Wallpaper() {
		return (EReference)screenEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScreen_Music() {
		return (EReference)screenEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActor() {
		return actorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActor_Position() {
		return (EAttribute)actorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImage() {
		return imageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImage_RealWidth() {
		return (EAttribute)imageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getImage_RealHeight() {
		return (EAttribute)imageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResource() {
		return resourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getResource_Path() {
		return (EAttribute)resourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMusic() {
		return musicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole() {
		return roleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Image() {
		return (EReference)roleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_PixelAction() {
		return (EReference)roleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_ActAction() {
		return (EReference)roleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_Collision() {
		return (EReference)roleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPixelAction() {
		return pixelActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPixelAction_Drag() {
		return (EAttribute)pixelActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPixelAction_Click() {
		return (EAttribute)pixelActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAction() {
		return actionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActAction() {
		return actActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActAction_Action() {
		return (EAttribute)actActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCollision() {
		return collisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollision_With() {
		return (EReference)collisionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCollision_CollisionType() {
		return (EReference)collisionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCollisionType() {
		return collisionTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomActor() {
		return customActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGameActor() {
		return gameActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGameActor_RoleFactory() {
		return (EReference)gameActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGameActor_Engine() {
		return (EReference)gameActorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoleFactory() {
		return roleFactoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoleFactory_Role() {
		return (EReference)roleFactoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoleFactory_TimeToCreate() {
		return (EAttribute)roleFactoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoleFactory_SlackTime() {
		return (EAttribute)roleFactoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoleFactory_XCreatedPosition() {
		return (EAttribute)roleFactoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoleFactory_YCreatedPosition() {
		return (EAttribute)roleFactoryEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEngine() {
		return engineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStatusBar() {
		return statusBarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWidget() {
		return widgetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getButton() {
		return buttonEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTextualWidget() {
		return textualWidgetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTextualWidget_Text() {
		return (EAttribute)textualWidgetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLabel() {
		return labelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getImageWidget() {
		return imageWidgetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getImageWidget_Image() {
		return (EReference)imageWidgetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNextScreenButton() {
		return nextScreenButtonEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNextScreenButton_NextScreen() {
		return (EReference)nextScreenButtonEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnnemyInstance() {
		return ennemyInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRealTimeEngine() {
		return realTimeEngineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTurnBasedEngine() {
		return turnBasedEngineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGrid2DActor() {
		return grid2DActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGrid2DActor_GridEntities() {
		return (EReference)grid2DActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGrid2DActor_GridWidth() {
		return (EAttribute)grid2DActorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGrid2DActor_GridHeight() {
		return (EAttribute)grid2DActorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGrid2DActor_GridColor() {
		return (EAttribute)grid2DActorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGrid2DActor_AddGridRole() {
		return (EReference)grid2DActorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPixelActor() {
		return pixelActorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPixelActor_PixelEntities() {
		return (EReference)pixelActorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPixelEntity() {
		return pixelEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPixelEntity_XPosition() {
		return (EAttribute)pixelEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPixelEntity_YPosition() {
		return (EAttribute)pixelEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPixelEntity_Role() {
		return (EReference)pixelEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEntity() {
		return entityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPixelRole() {
		return pixelRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGridEntity() {
		return gridEntityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGridEntity_AbsPosition() {
		return (EAttribute)gridEntityEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getGridEntity_OrdPosition() {
		return (EAttribute)gridEntityEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGridEntity_Role() {
		return (EReference)gridEntityEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGridRole() {
		return gridRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getGridRole_MoveGridActions() {
		return (EReference)gridRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMoveGridAction() {
		return moveGridActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoveGridAction_NbCasesMin() {
		return (EAttribute)moveGridActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoveGridAction_NbCasesMax() {
		return (EAttribute)moveGridActionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMoveGridAction_CanJumpOtherEntities() {
		return (EAttribute)moveGridActionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getGridAction() {
		return gridActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAddGridRole() {
		return addGridRoleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAddGridRole_Role() {
		return (EReference)addGridRoleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTable() {
		return tableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTable_Widgets() {
		return (EReference)tableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDestroyCollision() {
		return destroyCollisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getChangeScreenCollision() {
		return changeScreenCollisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getChangeScreenCollision_Screen() {
		return (EReference)changeScreenCollisionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomCollision() {
		return customCollisionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getQuitButton() {
		return quitButtonEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontalMoving() {
		return horizontalMovingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVerticalMoving() {
		return verticalMovingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDiagonalMoving() {
		return diagonalMovingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCustomMoving() {
		return customMovingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFont() {
		return fontEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPositionEnum() {
		return positionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getDragActionEnum() {
		return dragActionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getClickActionEnum() {
		return clickActionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getActActionEnum() {
		return actActionEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getXPosition() {
		return xPositionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getYPosition() {
		return yPositionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBooleanEnum() {
		return booleanEnumEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getColor() {
		return colorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getString() {
		return stringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getInteger() {
		return integerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibGdxMMFactory getLibGdxMMFactory() {
		return (LibGdxMMFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		gameEClass = createEClass(GAME);
		createEReference(gameEClass, GAME__SCREENS);
		createEAttribute(gameEClass, GAME__GAME_PACKAGE);
		createEReference(gameEClass, GAME__ROLES);
		createEAttribute(gameEClass, GAME__SDK_LOCATION);
		createEReference(gameEClass, GAME__RESOURCE);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		elementEClass = createEClass(ELEMENT);

		screenEClass = createEClass(SCREEN);
		createEReference(screenEClass, SCREEN__ACTORS);
		createEReference(screenEClass, SCREEN__WALLPAPER);
		createEReference(screenEClass, SCREEN__MUSIC);

		actorEClass = createEClass(ACTOR);
		createEAttribute(actorEClass, ACTOR__POSITION);

		imageEClass = createEClass(IMAGE);
		createEAttribute(imageEClass, IMAGE__REAL_WIDTH);
		createEAttribute(imageEClass, IMAGE__REAL_HEIGHT);

		resourceEClass = createEClass(RESOURCE);
		createEAttribute(resourceEClass, RESOURCE__PATH);

		musicEClass = createEClass(MUSIC);

		roleEClass = createEClass(ROLE);
		createEReference(roleEClass, ROLE__IMAGE);
		createEReference(roleEClass, ROLE__PIXEL_ACTION);
		createEReference(roleEClass, ROLE__ACT_ACTION);
		createEReference(roleEClass, ROLE__COLLISION);

		pixelActionEClass = createEClass(PIXEL_ACTION);
		createEAttribute(pixelActionEClass, PIXEL_ACTION__DRAG);
		createEAttribute(pixelActionEClass, PIXEL_ACTION__CLICK);

		actionEClass = createEClass(ACTION);

		actActionEClass = createEClass(ACT_ACTION);
		createEAttribute(actActionEClass, ACT_ACTION__ACTION);

		collisionEClass = createEClass(COLLISION);
		createEReference(collisionEClass, COLLISION__WITH);
		createEReference(collisionEClass, COLLISION__COLLISION_TYPE);

		collisionTypeEClass = createEClass(COLLISION_TYPE);

		customActorEClass = createEClass(CUSTOM_ACTOR);

		gameActorEClass = createEClass(GAME_ACTOR);
		createEReference(gameActorEClass, GAME_ACTOR__ROLE_FACTORY);
		createEReference(gameActorEClass, GAME_ACTOR__ENGINE);

		roleFactoryEClass = createEClass(ROLE_FACTORY);
		createEReference(roleFactoryEClass, ROLE_FACTORY__ROLE);
		createEAttribute(roleFactoryEClass, ROLE_FACTORY__TIME_TO_CREATE);
		createEAttribute(roleFactoryEClass, ROLE_FACTORY__SLACK_TIME);
		createEAttribute(roleFactoryEClass, ROLE_FACTORY__XCREATED_POSITION);
		createEAttribute(roleFactoryEClass, ROLE_FACTORY__YCREATED_POSITION);

		engineEClass = createEClass(ENGINE);

		statusBarEClass = createEClass(STATUS_BAR);

		widgetEClass = createEClass(WIDGET);

		buttonEClass = createEClass(BUTTON);

		textualWidgetEClass = createEClass(TEXTUAL_WIDGET);
		createEAttribute(textualWidgetEClass, TEXTUAL_WIDGET__TEXT);

		labelEClass = createEClass(LABEL);

		imageWidgetEClass = createEClass(IMAGE_WIDGET);
		createEReference(imageWidgetEClass, IMAGE_WIDGET__IMAGE);

		nextScreenButtonEClass = createEClass(NEXT_SCREEN_BUTTON);
		createEReference(nextScreenButtonEClass, NEXT_SCREEN_BUTTON__NEXT_SCREEN);

		ennemyInstanceEClass = createEClass(ENNEMY_INSTANCE);

		realTimeEngineEClass = createEClass(REAL_TIME_ENGINE);

		turnBasedEngineEClass = createEClass(TURN_BASED_ENGINE);

		grid2DActorEClass = createEClass(GRID2_DACTOR);
		createEReference(grid2DActorEClass, GRID2_DACTOR__GRID_ENTITIES);
		createEAttribute(grid2DActorEClass, GRID2_DACTOR__GRID_WIDTH);
		createEAttribute(grid2DActorEClass, GRID2_DACTOR__GRID_HEIGHT);
		createEAttribute(grid2DActorEClass, GRID2_DACTOR__GRID_COLOR);
		createEReference(grid2DActorEClass, GRID2_DACTOR__ADD_GRID_ROLE);

		pixelActorEClass = createEClass(PIXEL_ACTOR);
		createEReference(pixelActorEClass, PIXEL_ACTOR__PIXEL_ENTITIES);

		pixelEntityEClass = createEClass(PIXEL_ENTITY);
		createEAttribute(pixelEntityEClass, PIXEL_ENTITY__XPOSITION);
		createEAttribute(pixelEntityEClass, PIXEL_ENTITY__YPOSITION);
		createEReference(pixelEntityEClass, PIXEL_ENTITY__ROLE);

		entityEClass = createEClass(ENTITY);

		pixelRoleEClass = createEClass(PIXEL_ROLE);

		gridEntityEClass = createEClass(GRID_ENTITY);
		createEAttribute(gridEntityEClass, GRID_ENTITY__ABS_POSITION);
		createEAttribute(gridEntityEClass, GRID_ENTITY__ORD_POSITION);
		createEReference(gridEntityEClass, GRID_ENTITY__ROLE);

		gridRoleEClass = createEClass(GRID_ROLE);
		createEReference(gridRoleEClass, GRID_ROLE__MOVE_GRID_ACTIONS);

		moveGridActionEClass = createEClass(MOVE_GRID_ACTION);
		createEAttribute(moveGridActionEClass, MOVE_GRID_ACTION__NB_CASES_MIN);
		createEAttribute(moveGridActionEClass, MOVE_GRID_ACTION__NB_CASES_MAX);
		createEAttribute(moveGridActionEClass, MOVE_GRID_ACTION__CAN_JUMP_OTHER_ENTITIES);

		gridActionEClass = createEClass(GRID_ACTION);

		addGridRoleEClass = createEClass(ADD_GRID_ROLE);
		createEReference(addGridRoleEClass, ADD_GRID_ROLE__ROLE);

		tableEClass = createEClass(TABLE);
		createEReference(tableEClass, TABLE__WIDGETS);

		destroyCollisionEClass = createEClass(DESTROY_COLLISION);

		changeScreenCollisionEClass = createEClass(CHANGE_SCREEN_COLLISION);
		createEReference(changeScreenCollisionEClass, CHANGE_SCREEN_COLLISION__SCREEN);

		customCollisionEClass = createEClass(CUSTOM_COLLISION);

		quitButtonEClass = createEClass(QUIT_BUTTON);

		horizontalMovingEClass = createEClass(HORIZONTAL_MOVING);

		verticalMovingEClass = createEClass(VERTICAL_MOVING);

		diagonalMovingEClass = createEClass(DIAGONAL_MOVING);

		customMovingEClass = createEClass(CUSTOM_MOVING);

		fontEClass = createEClass(FONT);

		// Create enums
		positionEnumEEnum = createEEnum(POSITION_ENUM);
		dragActionEnumEEnum = createEEnum(DRAG_ACTION_ENUM);
		clickActionEnumEEnum = createEEnum(CLICK_ACTION_ENUM);
		actActionEnumEEnum = createEEnum(ACT_ACTION_ENUM);
		xPositionEEnum = createEEnum(XPOSITION);
		yPositionEEnum = createEEnum(YPOSITION);
		booleanEnumEEnum = createEEnum(BOOLEAN_ENUM);
		colorEEnum = createEEnum(COLOR);

		// Create data types
		stringEDataType = createEDataType(STRING);
		integerEDataType = createEDataType(INTEGER);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		gameEClass.getESuperTypes().add(this.getNamedElement());
		namedElementEClass.getESuperTypes().add(this.getElement());
		screenEClass.getESuperTypes().add(this.getNamedElement());
		actorEClass.getESuperTypes().add(this.getNamedElement());
		imageEClass.getESuperTypes().add(this.getResource());
		musicEClass.getESuperTypes().add(this.getResource());
		roleEClass.getESuperTypes().add(this.getNamedElement());
		pixelActionEClass.getESuperTypes().add(this.getAction());
		actionEClass.getESuperTypes().add(this.getElement());
		actActionEClass.getESuperTypes().add(this.getAction());
		collisionEClass.getESuperTypes().add(this.getAction());
		customActorEClass.getESuperTypes().add(this.getActor());
		gameActorEClass.getESuperTypes().add(this.getActor());
		engineEClass.getESuperTypes().add(this.getElement());
		statusBarEClass.getESuperTypes().add(this.getActor());
		buttonEClass.getESuperTypes().add(this.getTextualWidget());
		textualWidgetEClass.getESuperTypes().add(this.getWidget());
		labelEClass.getESuperTypes().add(this.getTextualWidget());
		imageWidgetEClass.getESuperTypes().add(this.getWidget());
		nextScreenButtonEClass.getESuperTypes().add(this.getButton());
		realTimeEngineEClass.getESuperTypes().add(this.getEngine());
		turnBasedEngineEClass.getESuperTypes().add(this.getEngine());
		grid2DActorEClass.getESuperTypes().add(this.getPixelActor());
		pixelActorEClass.getESuperTypes().add(this.getGameActor());
		pixelEntityEClass.getESuperTypes().add(this.getEntity());
		pixelRoleEClass.getESuperTypes().add(this.getRole());
		gridEntityEClass.getESuperTypes().add(this.getEntity());
		gridRoleEClass.getESuperTypes().add(this.getRole());
		moveGridActionEClass.getESuperTypes().add(this.getGridAction());
		gridActionEClass.getESuperTypes().add(this.getAction());
		addGridRoleEClass.getESuperTypes().add(this.getGridAction());
		tableEClass.getESuperTypes().add(this.getActor());
		destroyCollisionEClass.getESuperTypes().add(this.getCollisionType());
		changeScreenCollisionEClass.getESuperTypes().add(this.getCollisionType());
		customCollisionEClass.getESuperTypes().add(this.getCollisionType());
		quitButtonEClass.getESuperTypes().add(this.getButton());
		horizontalMovingEClass.getESuperTypes().add(this.getMoveGridAction());
		verticalMovingEClass.getESuperTypes().add(this.getMoveGridAction());
		diagonalMovingEClass.getESuperTypes().add(this.getMoveGridAction());
		customMovingEClass.getESuperTypes().add(this.getMoveGridAction());
		fontEClass.getESuperTypes().add(this.getResource());

		// Initialize classes, features, and operations; add parameters
		initEClass(gameEClass, Game.class, "Game", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGame_Screens(), this.getScreen(), null, "screens", null, 0, -1, Game.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getGame_GamePackage(), this.getString(), "gamePackage", null, 1, 1, Game.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGame_Roles(), this.getRole(), null, "roles", null, 0, -1, Game.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGame_SdkLocation(), this.getString(), "sdkLocation", null, 1, 1, Game.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGame_Resource(), this.getResource(), null, "resource", null, 0, -1, Game.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), this.getString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(elementEClass, Element.class, "Element", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(screenEClass, Screen.class, "Screen", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScreen_Actors(), this.getActor(), null, "actors", null, 0, -1, Screen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScreen_Wallpaper(), this.getImage(), null, "wallpaper", null, 1, 1, Screen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScreen_Music(), this.getMusic(), null, "music", null, 1, 1, Screen.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(actorEClass, Actor.class, "Actor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActor_Position(), this.getPositionEnum(), "position", null, 1, 1, Actor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(imageEClass, Image.class, "Image", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getImage_RealWidth(), this.getInteger(), "realWidth", null, 1, 1, Image.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getImage_RealHeight(), this.getInteger(), "realHeight", null, 1, 1, Image.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(resourceEClass, Resource.class, "Resource", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getResource_Path(), this.getString(), "path", null, 1, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(musicEClass, Music.class, "Music", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(roleEClass, Role.class, "Role", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_Image(), this.getImage(), null, "image", null, 1, 1, Role.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRole_PixelAction(), this.getPixelAction(), null, "pixelAction", null, 0, 1, Role.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRole_ActAction(), this.getActAction(), null, "actAction", null, 0, 1, Role.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRole_Collision(), this.getCollision(), null, "collision", null, 0, -1, Role.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(pixelActionEClass, PixelAction.class, "PixelAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPixelAction_Drag(), this.getDragActionEnum(), "drag", null, 1, 1, PixelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getPixelAction_Click(), this.getClickActionEnum(), "click", null, 1, 1, PixelAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(actionEClass, Action.class, "Action", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(actActionEClass, ActAction.class, "ActAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getActAction_Action(), this.getActActionEnum(), "action", null, 1, 1, ActAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(collisionEClass, Collision.class, "Collision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCollision_With(), this.getRole(), null, "with", null, 1, 1, Collision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getCollision_CollisionType(), this.getCollisionType(), null, "collisionType", null, 1, 1, Collision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(collisionTypeEClass, CollisionType.class, "CollisionType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(customActorEClass, CustomActor.class, "CustomActor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(gameActorEClass, GameActor.class, "GameActor", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGameActor_RoleFactory(), this.getRoleFactory(), null, "roleFactory", null, 0, -1, GameActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGameActor_Engine(), this.getEngine(), null, "engine", null, 1, 1, GameActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roleFactoryEClass, RoleFactory.class, "RoleFactory", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoleFactory_Role(), this.getRole(), null, "role", null, 1, 1, RoleFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoleFactory_TimeToCreate(), this.getInteger(), "timeToCreate", null, 1, 1, RoleFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoleFactory_SlackTime(), this.getInteger(), "slackTime", null, 1, 1, RoleFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoleFactory_XCreatedPosition(), this.getXPosition(), "xCreatedPosition", null, 1, 1, RoleFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoleFactory_YCreatedPosition(), this.getYPosition(), "yCreatedPosition", null, 1, 1, RoleFactory.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(engineEClass, Engine.class, "Engine", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(statusBarEClass, StatusBar.class, "StatusBar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(widgetEClass, Widget.class, "Widget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(buttonEClass, Button.class, "Button", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(textualWidgetEClass, TextualWidget.class, "TextualWidget", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTextualWidget_Text(), this.getString(), "text", null, 1, 1, TextualWidget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(labelEClass, Label.class, "Label", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(imageWidgetEClass, ImageWidget.class, "ImageWidget", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getImageWidget_Image(), this.getImage(), null, "image", null, 1, 1, ImageWidget.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(nextScreenButtonEClass, NextScreenButton.class, "NextScreenButton", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNextScreenButton_NextScreen(), this.getScreen(), null, "nextScreen", null, 1, 1, NextScreenButton.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(ennemyInstanceEClass, EnnemyInstance.class, "EnnemyInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(realTimeEngineEClass, RealTimeEngine.class, "RealTimeEngine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(turnBasedEngineEClass, TurnBasedEngine.class, "TurnBasedEngine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(grid2DActorEClass, Grid2DActor.class, "Grid2DActor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGrid2DActor_GridEntities(), this.getGridEntity(), null, "gridEntities", null, 0, -1, Grid2DActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGrid2DActor_GridWidth(), this.getInteger(), "gridWidth", null, 1, 1, Grid2DActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGrid2DActor_GridHeight(), this.getInteger(), "gridHeight", null, 1, 1, Grid2DActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGrid2DActor_GridColor(), this.getColor(), "gridColor", null, 1, 1, Grid2DActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGrid2DActor_AddGridRole(), this.getAddGridRole(), null, "addGridRole", null, 1, 1, Grid2DActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(pixelActorEClass, PixelActor.class, "PixelActor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPixelActor_PixelEntities(), this.getPixelEntity(), null, "pixelEntities", null, 0, -1, PixelActor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(pixelEntityEClass, PixelEntity.class, "PixelEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPixelEntity_XPosition(), this.getXPosition(), "xPosition", null, 1, 1, PixelEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getPixelEntity_YPosition(), this.getYPosition(), "yPosition", null, 1, 1, PixelEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getPixelEntity_Role(), this.getPixelRole(), null, "role", null, 1, 1, PixelEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(entityEClass, Entity.class, "Entity", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(pixelRoleEClass, PixelRole.class, "PixelRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(gridEntityEClass, GridEntity.class, "GridEntity", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getGridEntity_AbsPosition(), this.getInteger(), "absPosition", null, 1, 1, GridEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getGridEntity_OrdPosition(), this.getInteger(), "ordPosition", null, 1, 1, GridEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getGridEntity_Role(), this.getGridRole(), null, "role", null, 1, 1, GridEntity.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(gridRoleEClass, GridRole.class, "GridRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getGridRole_MoveGridActions(), this.getMoveGridAction(), null, "moveGridActions", null, 0, -1, GridRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(moveGridActionEClass, MoveGridAction.class, "MoveGridAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMoveGridAction_NbCasesMin(), this.getInteger(), "nbCasesMin", null, 1, 1, MoveGridAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getMoveGridAction_NbCasesMax(), this.getInteger(), "nbCasesMax", null, 1, 1, MoveGridAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getMoveGridAction_CanJumpOtherEntities(), this.getBooleanEnum(), "canJumpOtherEntities", null, 1, 1, MoveGridAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(gridActionEClass, GridAction.class, "GridAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(addGridRoleEClass, AddGridRole.class, "AddGridRole", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAddGridRole_Role(), this.getGridRole(), null, "role", null, 1, 1, AddGridRole.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(tableEClass, Table.class, "Table", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTable_Widgets(), this.getWidget(), null, "widgets", null, 0, -1, Table.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(destroyCollisionEClass, DestroyCollision.class, "DestroyCollision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(changeScreenCollisionEClass, ChangeScreenCollision.class, "ChangeScreenCollision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getChangeScreenCollision_Screen(), this.getScreen(), null, "screen", null, 1, 1, ChangeScreenCollision.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, !IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(customCollisionEClass, CustomCollision.class, "CustomCollision", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(quitButtonEClass, QuitButton.class, "QuitButton", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(horizontalMovingEClass, HorizontalMoving.class, "HorizontalMoving", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(verticalMovingEClass, VerticalMoving.class, "VerticalMoving", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(diagonalMovingEClass, DiagonalMoving.class, "DiagonalMoving", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(customMovingEClass, CustomMoving.class, "CustomMoving", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(fontEClass, Font.class, "Font", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(positionEnumEEnum, PositionEnum.class, "PositionEnum");
		addEEnumLiteral(positionEnumEEnum, PositionEnum.CENTER);
		addEEnumLiteral(positionEnumEEnum, PositionEnum.NORTH);
		addEEnumLiteral(positionEnumEEnum, PositionEnum.SOUTH);
		addEEnumLiteral(positionEnumEEnum, PositionEnum.EAST);
		addEEnumLiteral(positionEnumEEnum, PositionEnum.WEST);
		addEEnumLiteral(positionEnumEEnum, PositionEnum.CUSTOM);

		initEEnum(dragActionEnumEEnum, DragActionEnum.class, "DragActionEnum");
		addEEnumLiteral(dragActionEnumEEnum, DragActionEnum.MOVE_XY);
		addEEnumLiteral(dragActionEnumEEnum, DragActionEnum.MOVE_X);
		addEEnumLiteral(dragActionEnumEEnum, DragActionEnum.MOVE_Y);
		addEEnumLiteral(dragActionEnumEEnum, DragActionEnum.NOTHING);
		addEEnumLiteral(dragActionEnumEEnum, DragActionEnum.CUSTOM);

		initEEnum(clickActionEnumEEnum, ClickActionEnum.class, "ClickActionEnum");
		addEEnumLiteral(clickActionEnumEEnum, ClickActionEnum.DESTROY);
		addEEnumLiteral(clickActionEnumEEnum, ClickActionEnum.INC_SCORE);
		addEEnumLiteral(clickActionEnumEEnum, ClickActionEnum.DEC_SCORE);
		addEEnumLiteral(clickActionEnumEEnum, ClickActionEnum.NOTHING);
		addEEnumLiteral(clickActionEnumEEnum, ClickActionEnum.CUSTOM);

		initEEnum(actActionEnumEEnum, ActActionEnum.class, "ActActionEnum");
		addEEnumLiteral(actActionEnumEEnum, ActActionEnum.MOVE_UP);
		addEEnumLiteral(actActionEnumEEnum, ActActionEnum.MOVE_BOTTOM);
		addEEnumLiteral(actActionEnumEEnum, ActActionEnum.MOVE_LEFT);
		addEEnumLiteral(actActionEnumEEnum, ActActionEnum.MOVE_RIGHT);
		addEEnumLiteral(actActionEnumEEnum, ActActionEnum.CUSTOM);

		initEEnum(xPositionEEnum, XPosition.class, "XPosition");
		addEEnumLiteral(xPositionEEnum, XPosition.CENTER);
		addEEnumLiteral(xPositionEEnum, XPosition.LEFT);
		addEEnumLiteral(xPositionEEnum, XPosition.RIGHT);
		addEEnumLiteral(xPositionEEnum, XPosition.RANDOM);

		initEEnum(yPositionEEnum, YPosition.class, "YPosition");
		addEEnumLiteral(yPositionEEnum, YPosition.CENTER);
		addEEnumLiteral(yPositionEEnum, YPosition.TOP);
		addEEnumLiteral(yPositionEEnum, YPosition.BOTTOM);
		addEEnumLiteral(yPositionEEnum, YPosition.RANDOM);

		initEEnum(booleanEnumEEnum, BooleanEnum.class, "BooleanEnum");
		addEEnumLiteral(booleanEnumEEnum, BooleanEnum.TRUE);
		addEEnumLiteral(booleanEnumEEnum, BooleanEnum.FALSE);

		initEEnum(colorEEnum, Color.class, "Color");
		addEEnumLiteral(colorEEnum, Color.BLACK);
		addEEnumLiteral(colorEEnum, Color.GREY);
		addEEnumLiteral(colorEEnum, Color.RED);
		addEEnumLiteral(colorEEnum, Color.GREEN);
		addEEnumLiteral(colorEEnum, Color.BLUE);
		addEEnumLiteral(colorEEnum, Color.WHITE);
		addEEnumLiteral(colorEEnum, Color.NONE);

		// Initialize data types
		initEDataType(stringEDataType, String.class, "String", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(integerEDataType, Integer.class, "Integer", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //LibGdxMMPackageImpl
