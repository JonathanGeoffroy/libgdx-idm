/**
 */
package LibGdxMM.impl;

import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.NextScreenButton;
import LibGdxMM.Screen;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Next Screen Button</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link LibGdxMM.impl.NextScreenButtonImpl#getNextScreen <em>Next Screen</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class NextScreenButtonImpl extends ButtonImpl implements NextScreenButton {
	/**
	 * The cached value of the '{@link #getNextScreen() <em>Next Screen</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextScreen()
	 * @generated
	 * @ordered
	 */
	protected Screen nextScreen;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NextScreenButtonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibGdxMMPackage.Literals.NEXT_SCREEN_BUTTON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen getNextScreen() {
		if (nextScreen != null && nextScreen.eIsProxy()) {
			InternalEObject oldNextScreen = (InternalEObject)nextScreen;
			nextScreen = (Screen)eResolveProxy(oldNextScreen);
			if (nextScreen != oldNextScreen) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibGdxMMPackage.NEXT_SCREEN_BUTTON__NEXT_SCREEN, oldNextScreen, nextScreen));
			}
		}
		return nextScreen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Screen basicGetNextScreen() {
		return nextScreen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNextScreen(Screen newNextScreen) {
		Screen oldNextScreen = nextScreen;
		nextScreen = newNextScreen;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibGdxMMPackage.NEXT_SCREEN_BUTTON__NEXT_SCREEN, oldNextScreen, nextScreen));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibGdxMMPackage.NEXT_SCREEN_BUTTON__NEXT_SCREEN:
				if (resolve) return getNextScreen();
				return basicGetNextScreen();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibGdxMMPackage.NEXT_SCREEN_BUTTON__NEXT_SCREEN:
				setNextScreen((Screen)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibGdxMMPackage.NEXT_SCREEN_BUTTON__NEXT_SCREEN:
				setNextScreen((Screen)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibGdxMMPackage.NEXT_SCREEN_BUTTON__NEXT_SCREEN:
				return nextScreen != null;
		}
		return super.eIsSet(featureID);
	}

} //NextScreenButtonImpl
