/**
 */
package LibGdxMM.impl;

import LibGdxMM.Actor;
import LibGdxMM.Image;
import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.Music;
import LibGdxMM.Screen;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Screen</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link LibGdxMM.impl.ScreenImpl#getActors <em>Actors</em>}</li>
 *   <li>{@link LibGdxMM.impl.ScreenImpl#getWallpaper <em>Wallpaper</em>}</li>
 *   <li>{@link LibGdxMM.impl.ScreenImpl#getMusic <em>Music</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ScreenImpl extends NamedElementImpl implements Screen {
	/**
	 * The cached value of the '{@link #getActors() <em>Actors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActors()
	 * @generated
	 * @ordered
	 */
	protected EList<Actor> actors;

	/**
	 * The cached value of the '{@link #getWallpaper() <em>Wallpaper</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWallpaper()
	 * @generated
	 * @ordered
	 */
	protected Image wallpaper;

	/**
	 * The cached value of the '{@link #getMusic() <em>Music</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMusic()
	 * @generated
	 * @ordered
	 */
	protected Music music;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScreenImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LibGdxMMPackage.Literals.SCREEN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Actor> getActors() {
		if (actors == null) {
			actors = new EObjectContainmentEList<Actor>(Actor.class, this, LibGdxMMPackage.SCREEN__ACTORS);
		}
		return actors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Image getWallpaper() {
		if (wallpaper != null && wallpaper.eIsProxy()) {
			InternalEObject oldWallpaper = (InternalEObject)wallpaper;
			wallpaper = (Image)eResolveProxy(oldWallpaper);
			if (wallpaper != oldWallpaper) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibGdxMMPackage.SCREEN__WALLPAPER, oldWallpaper, wallpaper));
			}
		}
		return wallpaper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Image basicGetWallpaper() {
		return wallpaper;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWallpaper(Image newWallpaper) {
		Image oldWallpaper = wallpaper;
		wallpaper = newWallpaper;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibGdxMMPackage.SCREEN__WALLPAPER, oldWallpaper, wallpaper));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Music getMusic() {
		if (music != null && music.eIsProxy()) {
			InternalEObject oldMusic = (InternalEObject)music;
			music = (Music)eResolveProxy(oldMusic);
			if (music != oldMusic) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, LibGdxMMPackage.SCREEN__MUSIC, oldMusic, music));
			}
		}
		return music;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Music basicGetMusic() {
		return music;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMusic(Music newMusic) {
		Music oldMusic = music;
		music = newMusic;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, LibGdxMMPackage.SCREEN__MUSIC, oldMusic, music));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case LibGdxMMPackage.SCREEN__ACTORS:
				return ((InternalEList<?>)getActors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case LibGdxMMPackage.SCREEN__ACTORS:
				return getActors();
			case LibGdxMMPackage.SCREEN__WALLPAPER:
				if (resolve) return getWallpaper();
				return basicGetWallpaper();
			case LibGdxMMPackage.SCREEN__MUSIC:
				if (resolve) return getMusic();
				return basicGetMusic();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case LibGdxMMPackage.SCREEN__ACTORS:
				getActors().clear();
				getActors().addAll((Collection<? extends Actor>)newValue);
				return;
			case LibGdxMMPackage.SCREEN__WALLPAPER:
				setWallpaper((Image)newValue);
				return;
			case LibGdxMMPackage.SCREEN__MUSIC:
				setMusic((Music)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case LibGdxMMPackage.SCREEN__ACTORS:
				getActors().clear();
				return;
			case LibGdxMMPackage.SCREEN__WALLPAPER:
				setWallpaper((Image)null);
				return;
			case LibGdxMMPackage.SCREEN__MUSIC:
				setMusic((Music)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case LibGdxMMPackage.SCREEN__ACTORS:
				return actors != null && !actors.isEmpty();
			case LibGdxMMPackage.SCREEN__WALLPAPER:
				return wallpaper != null;
			case LibGdxMMPackage.SCREEN__MUSIC:
				return music != null;
		}
		return super.eIsSet(featureID);
	}

} //ScreenImpl
