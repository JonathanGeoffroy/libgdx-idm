/**
 */
package LibGdxMM.util;

import LibGdxMM.ActAction;
import LibGdxMM.Action;
import LibGdxMM.Actor;
import LibGdxMM.AddGridRole;
import LibGdxMM.Button;
import LibGdxMM.ChangeScreenCollision;
import LibGdxMM.Collision;
import LibGdxMM.CollisionType;
import LibGdxMM.CustomActor;
import LibGdxMM.CustomCollision;
import LibGdxMM.CustomMoving;
import LibGdxMM.DestroyCollision;
import LibGdxMM.DiagonalMoving;
import LibGdxMM.Element;
import LibGdxMM.Engine;
import LibGdxMM.EnnemyInstance;
import LibGdxMM.Entity;
import LibGdxMM.Font;
import LibGdxMM.Game;
import LibGdxMM.GameActor;
import LibGdxMM.Grid2DActor;
import LibGdxMM.GridAction;
import LibGdxMM.GridEntity;
import LibGdxMM.GridRole;
import LibGdxMM.HorizontalMoving;
import LibGdxMM.Image;
import LibGdxMM.ImageWidget;
import LibGdxMM.Label;
import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.MoveGridAction;
import LibGdxMM.Music;
import LibGdxMM.NamedElement;
import LibGdxMM.NextScreenButton;
import LibGdxMM.PixelAction;
import LibGdxMM.PixelActor;
import LibGdxMM.PixelEntity;
import LibGdxMM.PixelRole;
import LibGdxMM.QuitButton;
import LibGdxMM.RealTimeEngine;
import LibGdxMM.Resource;
import LibGdxMM.Role;
import LibGdxMM.RoleFactory;
import LibGdxMM.Screen;
import LibGdxMM.StatusBar;
import LibGdxMM.Table;
import LibGdxMM.TextualWidget;
import LibGdxMM.TurnBasedEngine;
import LibGdxMM.VerticalMoving;
import LibGdxMM.Widget;
import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see LibGdxMM.LibGdxMMPackage
 * @generated
 */
public class LibGdxMMAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LibGdxMMPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibGdxMMAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = LibGdxMMPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LibGdxMMSwitch<Adapter> modelSwitch =
		new LibGdxMMSwitch<Adapter>() {
			@Override
			public Adapter caseGame(Game object) {
				return createGameAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseElement(Element object) {
				return createElementAdapter();
			}
			@Override
			public Adapter caseScreen(Screen object) {
				return createScreenAdapter();
			}
			@Override
			public Adapter caseActor(Actor object) {
				return createActorAdapter();
			}
			@Override
			public Adapter caseImage(Image object) {
				return createImageAdapter();
			}
			@Override
			public Adapter caseResource(Resource object) {
				return createResourceAdapter();
			}
			@Override
			public Adapter caseMusic(Music object) {
				return createMusicAdapter();
			}
			@Override
			public Adapter caseRole(Role object) {
				return createRoleAdapter();
			}
			@Override
			public Adapter casePixelAction(PixelAction object) {
				return createPixelActionAdapter();
			}
			@Override
			public Adapter caseAction(Action object) {
				return createActionAdapter();
			}
			@Override
			public Adapter caseActAction(ActAction object) {
				return createActActionAdapter();
			}
			@Override
			public Adapter caseCollision(Collision object) {
				return createCollisionAdapter();
			}
			@Override
			public Adapter caseCollisionType(CollisionType object) {
				return createCollisionTypeAdapter();
			}
			@Override
			public Adapter caseCustomActor(CustomActor object) {
				return createCustomActorAdapter();
			}
			@Override
			public Adapter caseGameActor(GameActor object) {
				return createGameActorAdapter();
			}
			@Override
			public Adapter caseRoleFactory(RoleFactory object) {
				return createRoleFactoryAdapter();
			}
			@Override
			public Adapter caseEngine(Engine object) {
				return createEngineAdapter();
			}
			@Override
			public Adapter caseStatusBar(StatusBar object) {
				return createStatusBarAdapter();
			}
			@Override
			public Adapter caseWidget(Widget object) {
				return createWidgetAdapter();
			}
			@Override
			public Adapter caseButton(Button object) {
				return createButtonAdapter();
			}
			@Override
			public Adapter caseTextualWidget(TextualWidget object) {
				return createTextualWidgetAdapter();
			}
			@Override
			public Adapter caseLabel(Label object) {
				return createLabelAdapter();
			}
			@Override
			public Adapter caseImageWidget(ImageWidget object) {
				return createImageWidgetAdapter();
			}
			@Override
			public Adapter caseNextScreenButton(NextScreenButton object) {
				return createNextScreenButtonAdapter();
			}
			@Override
			public Adapter caseEnnemyInstance(EnnemyInstance object) {
				return createEnnemyInstanceAdapter();
			}
			@Override
			public Adapter caseRealTimeEngine(RealTimeEngine object) {
				return createRealTimeEngineAdapter();
			}
			@Override
			public Adapter caseTurnBasedEngine(TurnBasedEngine object) {
				return createTurnBasedEngineAdapter();
			}
			@Override
			public Adapter caseGrid2DActor(Grid2DActor object) {
				return createGrid2DActorAdapter();
			}
			@Override
			public Adapter casePixelActor(PixelActor object) {
				return createPixelActorAdapter();
			}
			@Override
			public Adapter casePixelEntity(PixelEntity object) {
				return createPixelEntityAdapter();
			}
			@Override
			public Adapter caseEntity(Entity object) {
				return createEntityAdapter();
			}
			@Override
			public Adapter casePixelRole(PixelRole object) {
				return createPixelRoleAdapter();
			}
			@Override
			public Adapter caseGridEntity(GridEntity object) {
				return createGridEntityAdapter();
			}
			@Override
			public Adapter caseGridRole(GridRole object) {
				return createGridRoleAdapter();
			}
			@Override
			public Adapter caseMoveGridAction(MoveGridAction object) {
				return createMoveGridActionAdapter();
			}
			@Override
			public Adapter caseGridAction(GridAction object) {
				return createGridActionAdapter();
			}
			@Override
			public Adapter caseAddGridRole(AddGridRole object) {
				return createAddGridRoleAdapter();
			}
			@Override
			public Adapter caseTable(Table object) {
				return createTableAdapter();
			}
			@Override
			public Adapter caseDestroyCollision(DestroyCollision object) {
				return createDestroyCollisionAdapter();
			}
			@Override
			public Adapter caseChangeScreenCollision(ChangeScreenCollision object) {
				return createChangeScreenCollisionAdapter();
			}
			@Override
			public Adapter caseCustomCollision(CustomCollision object) {
				return createCustomCollisionAdapter();
			}
			@Override
			public Adapter caseQuitButton(QuitButton object) {
				return createQuitButtonAdapter();
			}
			@Override
			public Adapter caseHorizontalMoving(HorizontalMoving object) {
				return createHorizontalMovingAdapter();
			}
			@Override
			public Adapter caseVerticalMoving(VerticalMoving object) {
				return createVerticalMovingAdapter();
			}
			@Override
			public Adapter caseDiagonalMoving(DiagonalMoving object) {
				return createDiagonalMovingAdapter();
			}
			@Override
			public Adapter caseCustomMoving(CustomMoving object) {
				return createCustomMovingAdapter();
			}
			@Override
			public Adapter caseFont(Font object) {
				return createFontAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Game <em>Game</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Game
	 * @generated
	 */
	public Adapter createGameAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Element <em>Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Element
	 * @generated
	 */
	public Adapter createElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Screen <em>Screen</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Screen
	 * @generated
	 */
	public Adapter createScreenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Actor <em>Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Actor
	 * @generated
	 */
	public Adapter createActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Image <em>Image</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Image
	 * @generated
	 */
	public Adapter createImageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Resource
	 * @generated
	 */
	public Adapter createResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Music <em>Music</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Music
	 * @generated
	 */
	public Adapter createMusicAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Role
	 * @generated
	 */
	public Adapter createRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.PixelAction <em>Pixel Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.PixelAction
	 * @generated
	 */
	public Adapter createPixelActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Action <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Action
	 * @generated
	 */
	public Adapter createActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.ActAction <em>Act Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.ActAction
	 * @generated
	 */
	public Adapter createActActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Collision <em>Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Collision
	 * @generated
	 */
	public Adapter createCollisionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.CollisionType <em>Collision Type</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.CollisionType
	 * @generated
	 */
	public Adapter createCollisionTypeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.CustomActor <em>Custom Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.CustomActor
	 * @generated
	 */
	public Adapter createCustomActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.GameActor <em>Game Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.GameActor
	 * @generated
	 */
	public Adapter createGameActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.RoleFactory <em>Role Factory</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.RoleFactory
	 * @generated
	 */
	public Adapter createRoleFactoryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Engine <em>Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Engine
	 * @generated
	 */
	public Adapter createEngineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.StatusBar <em>Status Bar</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.StatusBar
	 * @generated
	 */
	public Adapter createStatusBarAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Widget <em>Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Widget
	 * @generated
	 */
	public Adapter createWidgetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Button <em>Button</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Button
	 * @generated
	 */
	public Adapter createButtonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.TextualWidget <em>Textual Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.TextualWidget
	 * @generated
	 */
	public Adapter createTextualWidgetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Label <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Label
	 * @generated
	 */
	public Adapter createLabelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.ImageWidget <em>Image Widget</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.ImageWidget
	 * @generated
	 */
	public Adapter createImageWidgetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.NextScreenButton <em>Next Screen Button</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.NextScreenButton
	 * @generated
	 */
	public Adapter createNextScreenButtonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.EnnemyInstance <em>Ennemy Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.EnnemyInstance
	 * @generated
	 */
	public Adapter createEnnemyInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.RealTimeEngine <em>Real Time Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.RealTimeEngine
	 * @generated
	 */
	public Adapter createRealTimeEngineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.TurnBasedEngine <em>Turn Based Engine</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.TurnBasedEngine
	 * @generated
	 */
	public Adapter createTurnBasedEngineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Grid2DActor <em>Grid2 DActor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Grid2DActor
	 * @generated
	 */
	public Adapter createGrid2DActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.PixelActor <em>Pixel Actor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.PixelActor
	 * @generated
	 */
	public Adapter createPixelActorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.PixelEntity <em>Pixel Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.PixelEntity
	 * @generated
	 */
	public Adapter createPixelEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Entity
	 * @generated
	 */
	public Adapter createEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.PixelRole <em>Pixel Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.PixelRole
	 * @generated
	 */
	public Adapter createPixelRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.GridEntity <em>Grid Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.GridEntity
	 * @generated
	 */
	public Adapter createGridEntityAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.GridRole <em>Grid Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.GridRole
	 * @generated
	 */
	public Adapter createGridRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.MoveGridAction <em>Move Grid Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.MoveGridAction
	 * @generated
	 */
	public Adapter createMoveGridActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.GridAction <em>Grid Action</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.GridAction
	 * @generated
	 */
	public Adapter createGridActionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.AddGridRole <em>Add Grid Role</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.AddGridRole
	 * @generated
	 */
	public Adapter createAddGridRoleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Table <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Table
	 * @generated
	 */
	public Adapter createTableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.DestroyCollision <em>Destroy Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.DestroyCollision
	 * @generated
	 */
	public Adapter createDestroyCollisionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.ChangeScreenCollision <em>Change Screen Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.ChangeScreenCollision
	 * @generated
	 */
	public Adapter createChangeScreenCollisionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.CustomCollision <em>Custom Collision</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.CustomCollision
	 * @generated
	 */
	public Adapter createCustomCollisionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.QuitButton <em>Quit Button</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.QuitButton
	 * @generated
	 */
	public Adapter createQuitButtonAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.HorizontalMoving <em>Horizontal Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.HorizontalMoving
	 * @generated
	 */
	public Adapter createHorizontalMovingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.VerticalMoving <em>Vertical Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.VerticalMoving
	 * @generated
	 */
	public Adapter createVerticalMovingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.DiagonalMoving <em>Diagonal Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.DiagonalMoving
	 * @generated
	 */
	public Adapter createDiagonalMovingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.CustomMoving <em>Custom Moving</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.CustomMoving
	 * @generated
	 */
	public Adapter createCustomMovingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link LibGdxMM.Font <em>Font</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see LibGdxMM.Font
	 * @generated
	 */
	public Adapter createFontAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //LibGdxMMAdapterFactory
