/**
 */
package LibGdxMM.util;

import LibGdxMM.ActAction;
import LibGdxMM.Action;
import LibGdxMM.Actor;
import LibGdxMM.AddGridRole;
import LibGdxMM.Button;
import LibGdxMM.ChangeScreenCollision;
import LibGdxMM.Collision;
import LibGdxMM.CollisionType;
import LibGdxMM.CustomActor;
import LibGdxMM.CustomCollision;
import LibGdxMM.CustomMoving;
import LibGdxMM.DestroyCollision;
import LibGdxMM.DiagonalMoving;
import LibGdxMM.Element;
import LibGdxMM.Engine;
import LibGdxMM.EnnemyInstance;
import LibGdxMM.Entity;
import LibGdxMM.Font;
import LibGdxMM.Game;
import LibGdxMM.GameActor;
import LibGdxMM.Grid2DActor;
import LibGdxMM.GridAction;
import LibGdxMM.GridEntity;
import LibGdxMM.GridRole;
import LibGdxMM.HorizontalMoving;
import LibGdxMM.Image;
import LibGdxMM.ImageWidget;
import LibGdxMM.Label;
import LibGdxMM.LibGdxMMPackage;
import LibGdxMM.MoveGridAction;
import LibGdxMM.Music;
import LibGdxMM.NamedElement;
import LibGdxMM.NextScreenButton;
import LibGdxMM.PixelAction;
import LibGdxMM.PixelActor;
import LibGdxMM.PixelEntity;
import LibGdxMM.PixelRole;
import LibGdxMM.QuitButton;
import LibGdxMM.RealTimeEngine;
import LibGdxMM.Resource;
import LibGdxMM.Role;
import LibGdxMM.RoleFactory;
import LibGdxMM.Screen;
import LibGdxMM.StatusBar;
import LibGdxMM.Table;
import LibGdxMM.TextualWidget;
import LibGdxMM.TurnBasedEngine;
import LibGdxMM.VerticalMoving;
import LibGdxMM.Widget;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see LibGdxMM.LibGdxMMPackage
 * @generated
 */
public class LibGdxMMSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static LibGdxMMPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LibGdxMMSwitch() {
		if (modelPackage == null) {
			modelPackage = LibGdxMMPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case LibGdxMMPackage.GAME: {
				Game game = (Game)theEObject;
				T result = caseGame(game);
				if (result == null) result = caseNamedElement(game);
				if (result == null) result = caseElement(game);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.NAMED_ELEMENT: {
				NamedElement namedElement = (NamedElement)theEObject;
				T result = caseNamedElement(namedElement);
				if (result == null) result = caseElement(namedElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ELEMENT: {
				Element element = (Element)theEObject;
				T result = caseElement(element);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.SCREEN: {
				Screen screen = (Screen)theEObject;
				T result = caseScreen(screen);
				if (result == null) result = caseNamedElement(screen);
				if (result == null) result = caseElement(screen);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ACTOR: {
				Actor actor = (Actor)theEObject;
				T result = caseActor(actor);
				if (result == null) result = caseNamedElement(actor);
				if (result == null) result = caseElement(actor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.IMAGE: {
				Image image = (Image)theEObject;
				T result = caseImage(image);
				if (result == null) result = caseResource(image);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.RESOURCE: {
				Resource resource = (Resource)theEObject;
				T result = caseResource(resource);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.MUSIC: {
				Music music = (Music)theEObject;
				T result = caseMusic(music);
				if (result == null) result = caseResource(music);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ROLE: {
				Role role = (Role)theEObject;
				T result = caseRole(role);
				if (result == null) result = caseNamedElement(role);
				if (result == null) result = caseElement(role);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.PIXEL_ACTION: {
				PixelAction pixelAction = (PixelAction)theEObject;
				T result = casePixelAction(pixelAction);
				if (result == null) result = caseAction(pixelAction);
				if (result == null) result = caseElement(pixelAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ACTION: {
				Action action = (Action)theEObject;
				T result = caseAction(action);
				if (result == null) result = caseElement(action);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ACT_ACTION: {
				ActAction actAction = (ActAction)theEObject;
				T result = caseActAction(actAction);
				if (result == null) result = caseAction(actAction);
				if (result == null) result = caseElement(actAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.COLLISION: {
				Collision collision = (Collision)theEObject;
				T result = caseCollision(collision);
				if (result == null) result = caseAction(collision);
				if (result == null) result = caseElement(collision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.COLLISION_TYPE: {
				CollisionType collisionType = (CollisionType)theEObject;
				T result = caseCollisionType(collisionType);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.CUSTOM_ACTOR: {
				CustomActor customActor = (CustomActor)theEObject;
				T result = caseCustomActor(customActor);
				if (result == null) result = caseActor(customActor);
				if (result == null) result = caseNamedElement(customActor);
				if (result == null) result = caseElement(customActor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.GAME_ACTOR: {
				GameActor gameActor = (GameActor)theEObject;
				T result = caseGameActor(gameActor);
				if (result == null) result = caseActor(gameActor);
				if (result == null) result = caseNamedElement(gameActor);
				if (result == null) result = caseElement(gameActor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ROLE_FACTORY: {
				RoleFactory roleFactory = (RoleFactory)theEObject;
				T result = caseRoleFactory(roleFactory);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ENGINE: {
				Engine engine = (Engine)theEObject;
				T result = caseEngine(engine);
				if (result == null) result = caseElement(engine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.STATUS_BAR: {
				StatusBar statusBar = (StatusBar)theEObject;
				T result = caseStatusBar(statusBar);
				if (result == null) result = caseActor(statusBar);
				if (result == null) result = caseNamedElement(statusBar);
				if (result == null) result = caseElement(statusBar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.WIDGET: {
				Widget widget = (Widget)theEObject;
				T result = caseWidget(widget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.BUTTON: {
				Button button = (Button)theEObject;
				T result = caseButton(button);
				if (result == null) result = caseTextualWidget(button);
				if (result == null) result = caseWidget(button);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.TEXTUAL_WIDGET: {
				TextualWidget textualWidget = (TextualWidget)theEObject;
				T result = caseTextualWidget(textualWidget);
				if (result == null) result = caseWidget(textualWidget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.LABEL: {
				Label label = (Label)theEObject;
				T result = caseLabel(label);
				if (result == null) result = caseTextualWidget(label);
				if (result == null) result = caseWidget(label);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.IMAGE_WIDGET: {
				ImageWidget imageWidget = (ImageWidget)theEObject;
				T result = caseImageWidget(imageWidget);
				if (result == null) result = caseWidget(imageWidget);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.NEXT_SCREEN_BUTTON: {
				NextScreenButton nextScreenButton = (NextScreenButton)theEObject;
				T result = caseNextScreenButton(nextScreenButton);
				if (result == null) result = caseButton(nextScreenButton);
				if (result == null) result = caseTextualWidget(nextScreenButton);
				if (result == null) result = caseWidget(nextScreenButton);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ENNEMY_INSTANCE: {
				EnnemyInstance ennemyInstance = (EnnemyInstance)theEObject;
				T result = caseEnnemyInstance(ennemyInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.REAL_TIME_ENGINE: {
				RealTimeEngine realTimeEngine = (RealTimeEngine)theEObject;
				T result = caseRealTimeEngine(realTimeEngine);
				if (result == null) result = caseEngine(realTimeEngine);
				if (result == null) result = caseElement(realTimeEngine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.TURN_BASED_ENGINE: {
				TurnBasedEngine turnBasedEngine = (TurnBasedEngine)theEObject;
				T result = caseTurnBasedEngine(turnBasedEngine);
				if (result == null) result = caseEngine(turnBasedEngine);
				if (result == null) result = caseElement(turnBasedEngine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.GRID2_DACTOR: {
				Grid2DActor grid2DActor = (Grid2DActor)theEObject;
				T result = caseGrid2DActor(grid2DActor);
				if (result == null) result = casePixelActor(grid2DActor);
				if (result == null) result = caseGameActor(grid2DActor);
				if (result == null) result = caseActor(grid2DActor);
				if (result == null) result = caseNamedElement(grid2DActor);
				if (result == null) result = caseElement(grid2DActor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.PIXEL_ACTOR: {
				PixelActor pixelActor = (PixelActor)theEObject;
				T result = casePixelActor(pixelActor);
				if (result == null) result = caseGameActor(pixelActor);
				if (result == null) result = caseActor(pixelActor);
				if (result == null) result = caseNamedElement(pixelActor);
				if (result == null) result = caseElement(pixelActor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.PIXEL_ENTITY: {
				PixelEntity pixelEntity = (PixelEntity)theEObject;
				T result = casePixelEntity(pixelEntity);
				if (result == null) result = caseEntity(pixelEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ENTITY: {
				Entity entity = (Entity)theEObject;
				T result = caseEntity(entity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.PIXEL_ROLE: {
				PixelRole pixelRole = (PixelRole)theEObject;
				T result = casePixelRole(pixelRole);
				if (result == null) result = caseRole(pixelRole);
				if (result == null) result = caseNamedElement(pixelRole);
				if (result == null) result = caseElement(pixelRole);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.GRID_ENTITY: {
				GridEntity gridEntity = (GridEntity)theEObject;
				T result = caseGridEntity(gridEntity);
				if (result == null) result = caseEntity(gridEntity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.GRID_ROLE: {
				GridRole gridRole = (GridRole)theEObject;
				T result = caseGridRole(gridRole);
				if (result == null) result = caseRole(gridRole);
				if (result == null) result = caseNamedElement(gridRole);
				if (result == null) result = caseElement(gridRole);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.MOVE_GRID_ACTION: {
				MoveGridAction moveGridAction = (MoveGridAction)theEObject;
				T result = caseMoveGridAction(moveGridAction);
				if (result == null) result = caseGridAction(moveGridAction);
				if (result == null) result = caseAction(moveGridAction);
				if (result == null) result = caseElement(moveGridAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.GRID_ACTION: {
				GridAction gridAction = (GridAction)theEObject;
				T result = caseGridAction(gridAction);
				if (result == null) result = caseAction(gridAction);
				if (result == null) result = caseElement(gridAction);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.ADD_GRID_ROLE: {
				AddGridRole addGridRole = (AddGridRole)theEObject;
				T result = caseAddGridRole(addGridRole);
				if (result == null) result = caseGridAction(addGridRole);
				if (result == null) result = caseAction(addGridRole);
				if (result == null) result = caseElement(addGridRole);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.TABLE: {
				Table table = (Table)theEObject;
				T result = caseTable(table);
				if (result == null) result = caseActor(table);
				if (result == null) result = caseNamedElement(table);
				if (result == null) result = caseElement(table);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.DESTROY_COLLISION: {
				DestroyCollision destroyCollision = (DestroyCollision)theEObject;
				T result = caseDestroyCollision(destroyCollision);
				if (result == null) result = caseCollisionType(destroyCollision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.CHANGE_SCREEN_COLLISION: {
				ChangeScreenCollision changeScreenCollision = (ChangeScreenCollision)theEObject;
				T result = caseChangeScreenCollision(changeScreenCollision);
				if (result == null) result = caseCollisionType(changeScreenCollision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.CUSTOM_COLLISION: {
				CustomCollision customCollision = (CustomCollision)theEObject;
				T result = caseCustomCollision(customCollision);
				if (result == null) result = caseCollisionType(customCollision);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.QUIT_BUTTON: {
				QuitButton quitButton = (QuitButton)theEObject;
				T result = caseQuitButton(quitButton);
				if (result == null) result = caseButton(quitButton);
				if (result == null) result = caseTextualWidget(quitButton);
				if (result == null) result = caseWidget(quitButton);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.HORIZONTAL_MOVING: {
				HorizontalMoving horizontalMoving = (HorizontalMoving)theEObject;
				T result = caseHorizontalMoving(horizontalMoving);
				if (result == null) result = caseMoveGridAction(horizontalMoving);
				if (result == null) result = caseGridAction(horizontalMoving);
				if (result == null) result = caseAction(horizontalMoving);
				if (result == null) result = caseElement(horizontalMoving);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.VERTICAL_MOVING: {
				VerticalMoving verticalMoving = (VerticalMoving)theEObject;
				T result = caseVerticalMoving(verticalMoving);
				if (result == null) result = caseMoveGridAction(verticalMoving);
				if (result == null) result = caseGridAction(verticalMoving);
				if (result == null) result = caseAction(verticalMoving);
				if (result == null) result = caseElement(verticalMoving);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.DIAGONAL_MOVING: {
				DiagonalMoving diagonalMoving = (DiagonalMoving)theEObject;
				T result = caseDiagonalMoving(diagonalMoving);
				if (result == null) result = caseMoveGridAction(diagonalMoving);
				if (result == null) result = caseGridAction(diagonalMoving);
				if (result == null) result = caseAction(diagonalMoving);
				if (result == null) result = caseElement(diagonalMoving);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.CUSTOM_MOVING: {
				CustomMoving customMoving = (CustomMoving)theEObject;
				T result = caseCustomMoving(customMoving);
				if (result == null) result = caseMoveGridAction(customMoving);
				if (result == null) result = caseGridAction(customMoving);
				if (result == null) result = caseAction(customMoving);
				if (result == null) result = caseElement(customMoving);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case LibGdxMMPackage.FONT: {
				Font font = (Font)theEObject;
				T result = caseFont(font);
				if (result == null) result = caseResource(font);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Game</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Game</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGame(Game object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseElement(Element object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Screen</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Screen</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScreen(Screen object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActor(Actor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Image</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImage(Image object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResource(Resource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Music</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Music</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMusic(Music object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRole(Role object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pixel Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pixel Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePixelAction(PixelAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAction(Action object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Act Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Act Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActAction(ActAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Collision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCollision(Collision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Collision Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Collision Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCollisionType(CollisionType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomActor(CustomActor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Game Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Game Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGameActor(GameActor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Factory</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Factory</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoleFactory(RoleFactory object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Engine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Engine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEngine(Engine object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Status Bar</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Status Bar</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseStatusBar(StatusBar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Widget</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Widget</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWidget(Widget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Button</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Button</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseButton(Button object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Textual Widget</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Textual Widget</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTextualWidget(TextualWidget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Label</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Label</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLabel(Label object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Image Widget</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Image Widget</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseImageWidget(ImageWidget object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Next Screen Button</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Next Screen Button</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNextScreenButton(NextScreenButton object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Ennemy Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Ennemy Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEnnemyInstance(EnnemyInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Real Time Engine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Real Time Engine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRealTimeEngine(RealTimeEngine object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Turn Based Engine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Turn Based Engine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTurnBasedEngine(TurnBasedEngine object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Grid2 DActor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Grid2 DActor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGrid2DActor(Grid2DActor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pixel Actor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pixel Actor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePixelActor(PixelActor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pixel Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pixel Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePixelEntity(PixelEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Pixel Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Pixel Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePixelRole(PixelRole object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Grid Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Grid Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGridEntity(GridEntity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Grid Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Grid Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGridRole(GridRole object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Move Grid Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Move Grid Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMoveGridAction(MoveGridAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Grid Action</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Grid Action</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseGridAction(GridAction object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Add Grid Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Add Grid Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAddGridRole(AddGridRole object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Table</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Table</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseTable(Table object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Destroy Collision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Destroy Collision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDestroyCollision(DestroyCollision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Change Screen Collision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Change Screen Collision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseChangeScreenCollision(ChangeScreenCollision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Collision</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Collision</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomCollision(CustomCollision object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Quit Button</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Quit Button</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseQuitButton(QuitButton object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Horizontal Moving</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Horizontal Moving</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHorizontalMoving(HorizontalMoving object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Vertical Moving</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Vertical Moving</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVerticalMoving(VerticalMoving object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Diagonal Moving</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Diagonal Moving</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDiagonalMoving(DiagonalMoving object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Custom Moving</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Custom Moving</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCustomMoving(CustomMoving object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Font</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Font</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFont(Font object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //LibGdxMMSwitch
