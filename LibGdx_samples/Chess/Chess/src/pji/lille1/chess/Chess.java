package pji.lille1.chess;
import pji.lille1.chess.screens.ChessGame;

import com.badlogic.gdx.Game;
public class Chess extends Game {
	@Override
	public void create() {
		setScreen(new ChessGame(this));
	}
}
