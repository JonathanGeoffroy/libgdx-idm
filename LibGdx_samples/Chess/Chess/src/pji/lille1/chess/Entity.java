package pji.lille1.chess;

import pji.lille1.chess.roles.collisions.Acceptable;
import pji.lille1.chess.roles.collisions.Visitor;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public abstract class Entity extends Image implements Visitor, Acceptable {
	public Entity(TextureRegion reg) {
		super(reg);
	}

	public void move() {};

	public boolean overlaps(Entity other) {
		return getX() < other.getX() + other.getWidth() && 
				getX() + getWidth() > other.getX() && 
				getY() < other.getY() + other.getHeight() && 
				getY() + getHeight() > other.getY();
	}
}
