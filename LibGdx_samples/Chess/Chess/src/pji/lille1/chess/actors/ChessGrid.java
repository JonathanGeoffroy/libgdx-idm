package pji.lille1.chess.actors;

import pji.lille1.chess.Entity;
import pji.lille1.chess.GridEntity;
import pji.lille1.chess.roles.CavalierBlanc;
import pji.lille1.chess.roles.CavalierNoir;
import pji.lille1.chess.roles.DameBlanche;
import pji.lille1.chess.roles.DameNoire;
import pji.lille1.chess.roles.FouBlanc;
import pji.lille1.chess.roles.FouNoir;
import pji.lille1.chess.roles.PionBlanc;
import pji.lille1.chess.roles.PionNoir;
import pji.lille1.chess.roles.RoiBlanc;
import pji.lille1.chess.roles.RoiNoir;
import pji.lille1.chess.roles.TourBlanche;
import pji.lille1.chess.roles.TourNoire;
import pji.lille1.chess.screens.ChessGame;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;


public class ChessGrid extends GridEngineActor {
	private ChessGame screen;
	/**
	  * A private vector for rendering Grid
	  */
	private Vector2 vec;
	
	/**
	  * A private renderer for grid
	  */
	private ShapeRenderer shapeRenderer;
	
	
	
	public ChessGrid (final ChessGame screen) {
		super(screen.getGame(), 8, 8);
		vec = new Vector2();
		shapeRenderer = new ShapeRenderer();
		this.screen = screen;
	}
	
	public void show() {
		Stage stageScreen = screen.getStage();
		
		// Initialize all roles
		RoiNoir.init();
		
		PionBlanc.init();
		
		PionNoir.init();
		
		CavalierBlanc.init();
		
		FouBlanc.init();
		
		DameBlanche.init();
		
		RoiBlanc.init();
		
		TourNoire.init();
		
		TourBlanche.init();
		
		DameNoire.init();
		
		CavalierNoir.init();
		
		FouNoir.init();
		
		
		// Create all instances of all roles
		float cellWidth = getWidth() / 8;
		float cellHeight = getHeight() / 8;
		GridEntity entityActor;
		
		
		entityActor = new CavalierNoir();
		entityActor.setBounds(
			6 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 6, 0);
		entities.add(entityActor);
		addListener(((CavalierNoir)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			2 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 2, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new CavalierBlanc();
		entityActor.setBounds(
			1 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 1, 7);
		entities.add(entityActor);
		addListener(((CavalierBlanc)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			0 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 0, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			5 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 5, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new TourBlanche();
		entityActor.setBounds(
			0 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 0, 7);
		entities.add(entityActor);
		addListener(((TourBlanche)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			2 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 2, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			7 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 7, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new DameNoire();
		entityActor.setBounds(
			3 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 3, 0);
		entities.add(entityActor);
		addListener(((DameNoire)entityActor).dragListener(this));
		
		entityActor = new CavalierNoir();
		entityActor.setBounds(
			1 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 1, 0);
		entities.add(entityActor);
		addListener(((CavalierNoir)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			4 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 4, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new FouNoir();
		entityActor.setBounds(
			2 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 2, 0);
		entities.add(entityActor);
		addListener(((FouNoir)entityActor).dragListener(this));
		
		entityActor = new TourNoire();
		entityActor.setBounds(
			0 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 0, 0);
		entities.add(entityActor);
		addListener(((TourNoire)entityActor).dragListener(this));
		
		entityActor = new RoiNoir();
		entityActor.setBounds(
			4 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 4, 0);
		entities.add(entityActor);
		addListener(((RoiNoir)entityActor).dragListener(this));
		
		entityActor = new CavalierBlanc();
		entityActor.setBounds(
			6 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 6, 7);
		entities.add(entityActor);
		addListener(((CavalierBlanc)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			3 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 3, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new DameBlanche();
		entityActor.setBounds(
			3 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 3, 7);
		entities.add(entityActor);
		addListener(((DameBlanche)entityActor).dragListener(this));
		
		entityActor = new FouBlanc();
		entityActor.setBounds(
			2 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 2, 7);
		entities.add(entityActor);
		addListener(((FouBlanc)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			0 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 0, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			3 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 3, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			1 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 1, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			7 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 7, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new TourBlanche();
		entityActor.setBounds(
			7 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 7, 7);
		entities.add(entityActor);
		addListener(((TourBlanche)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			6 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 6, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			6 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 6, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new FouNoir();
		entityActor.setBounds(
			5 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 5, 0);
		entities.add(entityActor);
		addListener(((FouNoir)entityActor).dragListener(this));
		
		entityActor = new TourNoire();
		entityActor.setBounds(
			7 * cellWidth,
			(8 - 0 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 7, 0);
		entities.add(entityActor);
		addListener(((TourNoire)entityActor).dragListener(this));
		
		entityActor = new PionBlanc();
		entityActor.setBounds(
			1 * cellWidth,
			(8 - 6 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 1, 6);
		entities.add(entityActor);
		addListener(((PionBlanc)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			5 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 5, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new PionNoir();
		entityActor.setBounds(
			4 * cellWidth,
			(8 - 1 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 4, 1);
		entities.add(entityActor);
		addListener(((PionNoir)entityActor).dragListener(this));
		
		entityActor = new FouBlanc();
		entityActor.setBounds(
			5 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 5, 7);
		entities.add(entityActor);
		addListener(((FouBlanc)entityActor).dragListener(this));
		
		entityActor = new RoiBlanc();
		entityActor.setBounds(
			4 * cellWidth,
			(8 - 7 - 1) * cellHeight,
			cellWidth,
			cellHeight
		);
		setEntityAt(entityActor, 4, 7);
		entities.add(entityActor);
		addListener(((RoiBlanc)entityActor).dragListener(this));
		
	}
	
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		for(Entity e : entities) {
			e.draw(batch, parentAlpha);
		}
		// Draw the grid of the game
		float cellWidth = getWidth() / gridEntities[0].length;
		float cellHeight = getHeight() / gridEntities.length;
		batch.end();
		
		vec.set(getX(), getY());
		this.localToStageCoordinates(vec);
		shapeRenderer.begin(ShapeType.Line);
			shapeRenderer.setColor(Color.WHITE);
			// Draw all vertical lines
			for (int i = 0; i <= gridEntities.length; i++) {
				shapeRenderer.line(i * cellWidth, 0, i * cellWidth, getHeight());
			}
			
			// Draw all horizontal lines
			for (int i = 0; i <= gridEntities.length; i++) {
				shapeRenderer.line(0, i * cellHeight, getWidth(), i * cellHeight);
			}
		shapeRenderer.end();
		
		batch.begin();
	}
	
	public void act(float deltaTime) {
		super.act(deltaTime);
		Stage stage = screen.getStage();
		
	}
	
	public void dispose() {
		
	}	
	
	public void collisions() {
		Entity first, other;
		for(int i = entities.size() - 1; i > 0; i--) {
			first = entities.get(i);
			for(int j = i - 1; j >= 0; j--) {
				other = entities.get(j);
				if(first.overlaps(other)) {
					first.accept(this, other);
					other.accept(this, first);
				}
			}
		}
	}
}
