package pji.lille1.chess.actors;

import java.util.ArrayList;
import java.util.List;

import pji.lille1.chess.Entity;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.scenes.scene2d.Actor;

public abstract class EngineActor extends Actor {
	protected Game game;

	/**
	 * All entities in the screen
	 */
	protected List<Entity> entities;
	
	public EngineActor(Game game) {	
		super();
		this.game = game;
		entities = new ArrayList<Entity>();
	}

	public List<Entity> getEntities() {
		return entities;
	}

	public void removeEntity(Entity entity) {
		entities.remove(entity);
	}

	public Game getGame() {
		return game;
	}
}
