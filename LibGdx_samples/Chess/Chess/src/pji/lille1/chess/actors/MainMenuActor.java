package pji.lille1.chess.actors;

import pji.lille1.chess.screens.ChessGame;
import pji.lille1.chess.screens.MainMenu;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

public class MainMenuActor extends Table {
	private MainMenu screen;
	private Skin skin;
	
	public MainMenuActor (final MainMenu screen) {
		/* Load the skin of the menu */
		skin = new Skin(Gdx.files.internal("data/ui/skins/skin.json"));
		setSkin(skin);
		this.screen = screen;
	}
	
	public void show() {
		Actor widget;
		
		/* Create each component & add it to the Table */
		widget = new TextButton("Play", skin);
		add(widget);
		widget.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Game game = screen.getGame(); 
				game.setScreen(new ChessGame(game));
			}
		});
		row();
		
		pack();
	}
	
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}
	
	public void act(float deltaTime) {
		super.act(deltaTime);
		
	}
	
	public void dispose() {
		
	}
}
