package pji.lille1.chess.roles;

import pji.lille1.chess.GridEntity;
import pji.lille1.chess.actors.EngineActor;
import pji.lille1.chess.roles.collisions.Visitor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class CavalierNoir extends GridEntity {
	private static Texture texture;
	private static TextureRegion textureRegion;
	

	public CavalierNoir() {
		super(textureRegion);
		
	}

	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}

	public void act(float delta) {
		super.act(delta);
		
	}

	public static void dispose() {
		texture.dispose();
		texture = null;
		textureRegion = null;
	}

	public static void init() {
		if(texture == null) {
			texture = new Texture(Gdx.files.internal("data/actors/images/cavalier_noir.png"));
			textureRegion = new TextureRegion(texture);
		}
	}

	public boolean canMoveAt(GridEntity[][] grid, int fromX, int fromY, int toX, int toY) {
		// TODO: create your own moving test
		return false;
	}

	

	public void accept(EngineActor sender, Visitor v) {
		v.collision(sender, this);
	}
	
	/**
	  * Manage Collision with FouBlanc
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, FouBlanc fouBlanc) {}
	
	/**
	  * Manage Collision with DameBlanche
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, DameBlanche dameBlanche) {}
	
	/**
	  * Manage Collision with RoiNoir
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, RoiNoir roiNoir) {}
	
	/**
	  * Manage Collision with TourNoire
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, TourNoire tourNoire) {}
	
	/**
	  * Manage Collision with RoiBlanc
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, RoiBlanc roiBlanc) {}
	
	/**
	  * Manage Collision with TourBlanche
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, TourBlanche tourBlanche) {}
	
	/**
	  * Manage Collision with DameNoire
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, DameNoire dameNoire) {}
	
	/**
	  * Manage Collision with CavalierNoir
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, CavalierNoir cavalierNoir) {}
	
	/**
	  * Manage Collision with PionBlanc
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, PionBlanc pionBlanc) {}
	
	/**
	  * Manage Collision with PionNoir
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, PionNoir pionNoir) {}
	
	/**
	  * Manage Collision with FouNoir
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, FouNoir fouNoir) {}
	
	/**
	  * Manage Collision with CavalierBlanc
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, CavalierBlanc cavalierBlanc) {}
	
	
}
