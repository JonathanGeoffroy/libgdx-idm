package pji.lille1.chess.roles.collisions;

import pji.lille1.chess.actors.EngineActor;

public interface Acceptable {
	void accept(EngineActor sender, Visitor v);
}
