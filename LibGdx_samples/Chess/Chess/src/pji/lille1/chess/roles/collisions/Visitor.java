package pji.lille1.chess.roles.collisions;
import pji.lille1.chess.actors.EngineActor;
import pji.lille1.chess.roles.CavalierBlanc;
import pji.lille1.chess.roles.CavalierNoir;
import pji.lille1.chess.roles.DameBlanche;
import pji.lille1.chess.roles.DameNoire;
import pji.lille1.chess.roles.FouBlanc;
import pji.lille1.chess.roles.FouNoir;
import pji.lille1.chess.roles.PionBlanc;
import pji.lille1.chess.roles.PionNoir;
import pji.lille1.chess.roles.RoiBlanc;
import pji.lille1.chess.roles.RoiNoir;
import pji.lille1.chess.roles.TourBlanche;
import pji.lille1.chess.roles.TourNoire;

public interface Visitor {
	void collision(EngineActor sender, FouBlanc fouBlanc);
	void collision(EngineActor sender, DameBlanche dameBlanche);
	void collision(EngineActor sender, RoiNoir roiNoir);
	void collision(EngineActor sender, RoiBlanc roiBlanc);
	void collision(EngineActor sender, TourNoire tourNoire);
	void collision(EngineActor sender, TourBlanche tourBlanche);
	void collision(EngineActor sender, PionBlanc pionBlanc);
	void collision(EngineActor sender, CavalierNoir cavalierNoir);
	void collision(EngineActor sender, DameNoire dameNoire);
	void collision(EngineActor sender, PionNoir pionNoir);
	void collision(EngineActor sender, CavalierBlanc cavalierBlanc);
	void collision(EngineActor sender, FouNoir fouNoir);
}
