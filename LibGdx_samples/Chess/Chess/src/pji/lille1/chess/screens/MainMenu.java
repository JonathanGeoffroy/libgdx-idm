package pji.lille1.chess.screens;

import pji.lille1.chess.actors.MainMenuActor;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;


public class MainMenu implements Screen {
	/**
	  * A reference to the game which is running
	  */
	private Game game;
	
	/**
	  * The stage which contains all Actors of this Screen
	  */
	private Stage stage;
	
	
	
	

	public MainMenu(Game game) {
		super();
		this.game = game;
		this.stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
	}

	@Override
	public void show() {
		
		
		
		
		Actor currentActor;
		currentActor = new MainMenuActor(this);
		((MainMenuActor) currentActor).show();
		// WEST
		currentActor.setBounds(0, 0, Gdx.graphics.getWidth() / 10.f, Gdx.graphics.getHeight());
		stage.addActor(currentActor);
		
	
		/*
	     * The stage will catch all player events
	     */
		Gdx.input.setInputProcessor(stage);
	}

	/**
	  * Free the memory from all ressources loaded by show()
	  * This method is automatically called by LibGdx internal system when the game is paused.
	  * For example, on tablets, this method's called when the user-player swicth from this game to another Android App
	  */
	@Override
	public void dispose() {
		
		
		stage.dispose();
	}

	public void render(float delta){
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();;
	}

		@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
		show();		
	}

	public Stage getStage() {
		return stage;
	}
	
	public Game getGame() {
		return game;
	}
}
