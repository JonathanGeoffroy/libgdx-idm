package pji.lille1.humdonuts;

import pji.lille1.humdonuts.actors.GridEngineActor;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

public abstract class GridEntity extends Entity {
	public GridEntity(TextureRegion reg) {
		super(reg);
	}

	public abstract boolean canMoveAt(GridEntity[][] grid, int fromX, int fromY, int toX, int toY);

	/**
  	  * Create a new Listener for Drag events
  	*/
	public DragListener dragListener(final GridEngineActor act) {
		DragListener onDrag = new DragListener() {
			private boolean dragged;
			private GridEngineActor actor = act;
			private float fromX, fromY;

			@Override
			public void dragStart(InputEvent event, float x, float y,
					int pointer) {
				super.dragStart(event, x, y, pointer);
				if (x >= getX() && x <= getX() + getWidth() &&
					y >= getY() && y <= getY() + getHeight()) {
					dragged = true;
					fromX = x;
					fromY = y;
				}
			}
	
			@Override
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				super.dragStop(event, x, y, pointer);
				if(dragged) {
					dragged = false;
					actor.moveEntity(fromX, fromY, x, y);
				}
			}
			
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				if (dragged) {
					setPosition(x - getWidth() / 2, y - getHeight() /2);
				}
			}		
		};
		return onDrag;
	}
} 
