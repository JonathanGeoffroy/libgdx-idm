package pji.lille1.humdonuts;
import pji.lille1.humdonuts.screens.MainMenuScreen;

import com.badlogic.gdx.Game;
public class HumDonuts extends Game {
	@Override
	public void create() {
		setScreen(new MainMenuScreen(this));
	}
}
