package pji.lille1.humdonuts;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

public abstract class PixelEntity extends Entity {
	public PixelEntity(TextureRegion reg) {
		super(reg);
	}
}
