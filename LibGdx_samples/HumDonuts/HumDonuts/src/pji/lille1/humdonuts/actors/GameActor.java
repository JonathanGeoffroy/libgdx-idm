package pji.lille1.humdonuts.actors;

import pji.lille1.humdonuts.Entity;
import pji.lille1.humdonuts.PixelEntity;
import pji.lille1.humdonuts.roles.Donut;
import pji.lille1.humdonuts.roles.Homer;
import pji.lille1.humdonuts.roles.PoisonDonut;
import pji.lille1.humdonuts.screens.GameDonuts;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class GameActor extends EngineActor {
	private GameDonuts screen;
	/** 
	  * the number of elapsed *act* method since the last creation of PoisonDonut
	  **/
	private int nbAct_PoisonDonut;
	
	/** 
	  * the number of elapsed *act* method since the last creation of Donut
	  **/
	private int nbAct_Donut;
	
	
	
	
	public GameActor (final GameDonuts screen) {
		super(screen.getGame());
		this.screen = screen;
	}
	
	public void show() {
		Stage stageScreen = screen.getStage();
		
		// Initialize all roles
		Homer.init();
		
		Donut.init();
		
		PoisonDonut.init();
		
		
		// Create all instances of all roles
		PixelEntity entityActor;
		entityActor = new Homer();
		entities.add(entityActor);
		entityActor.setBounds(
			getWidth() * 5 / 12,
			0,
			this.getWidth() / 12,
			this.getHeight() / 12
		);
		addListener(((Homer)entityActor).dragListener());
		
	}
	
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		for(Entity e : entities) {
			e.draw(batch, parentAlpha);
		}
	}
	
	public void act(float deltaTime) {
		super.act(deltaTime);
		Stage stage = screen.getStage();
		
		/* remove all entities wich aren't into the screen */
		for(int i = entities.size() - 1; i > 0; i--) {
			Image im = entities.get(i);
			if(im.getX() + im.getWidth() > this.getWidth() ||im.getX() < 0 ||
		       im.getY() > this.getHeight() ||im.getY() +im.getHeight() < 0) {
				removeEntity(entities.get(i));
			}
		}
		
		/* Move all entities */
		for(Entity e : entities) {
			e.move();
		}
		
		/* Create a new PoisonDonut if it's time */
		if(nbAct_PoisonDonut >= 80) {
			if(nbAct_PoisonDonut >= 130 ||
		       Math.random() >= 100.f/50) {
				PoisonDonut factoryEntity = new PoisonDonut();
				factoryEntity.setBounds(
					(float)(Math.random() * (getWidth() * 11 / 12)),
					getHeight() * 11 / 12,
					getWidth() / 12,
					getHeight() / 12
				); 
				entities.add(factoryEntity);
				nbAct_PoisonDonut = 0;
			}
		}
		nbAct_PoisonDonut++;
		
		/* Create a new Donut if it's time */
		if(nbAct_Donut >= 30) {
			if(nbAct_Donut >= 38 ||
		       Math.random() >= 100.f/8) {
				Donut factoryEntity = new Donut();
				factoryEntity.setBounds(
					(float)(Math.random() * (getWidth() * 11 / 12)),
					getHeight() * 11 / 12,
					getWidth() / 12,
					getHeight() / 12
				); 
				entities.add(factoryEntity);
				nbAct_Donut = 0;
			}
		}
		nbAct_Donut++;
		
		/* Test all collisions for each entity  */
		collisions();
	}
	
	public void dispose() {
		
	}	
	
	public void collisions() {
		Entity first, other;
		for(int i = entities.size() - 1; i > 0; i--) {
			first = entities.get(i);
			for(int j = i - 1; j >= 0; j--) {
				other = entities.get(j);
				if(first.overlaps(other)) {
					first.accept(this, other);
					other.accept(this, first);
				}
			}
		}
	}
}
