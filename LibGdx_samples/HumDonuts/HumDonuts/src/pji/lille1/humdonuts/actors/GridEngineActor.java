package pji.lille1.humdonuts.actors;

import pji.lille1.humdonuts.Entity;
import pji.lille1.humdonuts.GridEntity;

import com.badlogic.gdx.Game;

public abstract class GridEngineActor extends EngineActor {
	/**
	  * A 2D grid, containing all entities
	  * A Case can contains only 1 entity, or can be null if there is no entity at this case
	  */
	protected GridEntity[][] gridEntities;

	public GridEngineActor(Game game, int sizeX, int sizeY) {
		super(game);
		gridEntities = new GridEntity[sizeY][sizeX];
	}

	@Override
	public void removeEntity(Entity entity) {
		entities.remove(entity);
		for(int i = 0; i < gridEntities.length; i++) {
			for(int j = 0; j < gridEntities[i].length; j++) {
				if(gridEntities[i][j].equals(entity)) {
					gridEntities[i][j] = null;
					return;
				}
			}
		}
	}

	public void setEntityAt(GridEntity entity, int abs, int ord) {
		gridEntities[ord][abs] = entity;
	}

	public void moveEntity(float pixFromX, float pixFromY, float pixToX, float pixToY) {
		float caseWidth = getWidth() / gridEntities[0].length;
		float caseHeight = getHeight() / gridEntities.length;
		int fromY = gridEntities.length - (int) (pixFromY / caseHeight) - 1;
		int fromX = (int) (pixFromX / caseWidth);
		int toY = gridEntities.length - (int) (pixToY / caseHeight) - 1;
		int toX = (int) (pixToX / caseWidth);
		GridEntity movedEntity = gridEntities[fromY][fromX];
		
		if(movedEntity.canMoveAt(gridEntities, fromX, fromY, toX, toY)) {
			// Remove entity at the toX-toY place
			if(gridEntities[toY][toX] != null) {
				entities.remove(gridEntities[toY][toX]);
				gridEntities[toY][toX].setPosition(9999, 9999);
				gridEntities[toY][toX] = null;
			}
			// Move entity at toX-toY
			gridEntities[fromY][fromX] = null;
			gridEntities[toY][toX] = movedEntity;
			movedEntity.setPosition(toX * caseWidth, getHeight() - (toY+1) * caseHeight);
		}
		else {
			// Replace entity a it old place
			movedEntity.setPosition(fromX * caseWidth, getHeight() - (fromY+1) * caseHeight);
		}
	} 
}
