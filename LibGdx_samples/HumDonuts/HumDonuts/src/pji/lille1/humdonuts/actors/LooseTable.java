package pji.lille1.humdonuts.actors;

import pji.lille1.humdonuts.screens.GameDonuts;
import pji.lille1.humdonuts.screens.LooseScreen;
import pji.lille1.humdonuts.screens.MainMenuScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;

public class LooseTable extends Table {
	private LooseScreen screen;
	private Skin skin;
	
	public LooseTable (final LooseScreen screen) {
		/* Load the skin of the menu */
		skin = new Skin(Gdx.files.internal("data/ui/skins/skin.json"));
		setSkin(skin);
		this.screen = screen;
	}
	
	public void show() {
		Actor widget;
		
		/* Create each component & add it to the Table */
		add("Vous avez perdu ...");
		row();
		
		widget = new TextButton("Rejouer", skin);
		add(widget);
		widget.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Game game = screen.getGame(); 
				game.setScreen(new GameDonuts(game));
			}
		});
		row();
		
		widget = new TextButton("Menu Principal", skin);
		add(widget);
		widget.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Game game = screen.getGame(); 
				game.setScreen(new MainMenuScreen(game));
			}
		});
		row();
		
		widget = new TextButton("Quitter", skin);
		add(widget);
		widget.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();
			}
		});
		row();
		
		pack();
	}
	
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}
	
	public void act(float deltaTime) {
		super.act(deltaTime);
		
	}
	
	public void dispose() {
		
	}
}
