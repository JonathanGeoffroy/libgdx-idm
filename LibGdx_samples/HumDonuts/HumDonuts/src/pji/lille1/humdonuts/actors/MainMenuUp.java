package pji.lille1.humdonuts.actors;

import pji.lille1.humdonuts.screens.MainMenuScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class MainMenuUp extends Table {
	private MainMenuScreen screen;
	private Skin skin;
	
	public MainMenuUp (final MainMenuScreen screen) {
		/* Load the skin of the menu */
		skin = new Skin(Gdx.files.internal("data/ui/skins/skin.json"));
		setSkin(skin);
		this.screen = screen;
	}
	
	public void show() {
		Actor widget;
		
		/* Create each component & add it to the Table */
		add("Hummmm Donut !!!");
		row();
		
		pack();
	}
	
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}
	
	public void act(float deltaTime) {
		super.act(deltaTime);
		
	}
	
	public void dispose() {
		
	}
}
