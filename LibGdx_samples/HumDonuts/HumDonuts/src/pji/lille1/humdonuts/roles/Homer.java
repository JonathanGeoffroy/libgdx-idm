package pji.lille1.humdonuts.roles;

import pji.lille1.humdonuts.PixelEntity;
import pji.lille1.humdonuts.actors.EngineActor;
import pji.lille1.humdonuts.roles.collisions.Visitor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;

public class Homer extends PixelEntity {
	private static Texture texture;
	private static TextureRegion textureRegion;
	

	public Homer() {
		super(textureRegion);
		
	}

	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}

	public void act(float delta) {
		super.act(delta);
		
	}

	public static void dispose() {
		texture.dispose();
		texture = null;
		textureRegion = null;
	}

	public static void init() {
		if(texture == null) {
			texture = new Texture(Gdx.files.internal("data/actors/images/homer.png"));
			textureRegion = new TextureRegion(texture);
		}
	}

	public void move(){
		
	}

	/**
	  * Create a new Listener for Drag events
	  */
	public DragListener dragListener() {
		DragListener onDrag = new DragListener() {
			private boolean dragged;
	
			@Override
			public void dragStart(InputEvent event, float x, float y,
					int pointer) {
				super.dragStart(event, x, y, pointer);
				if (x >= getX() && x <= getX() + getWidth() &&
					y >= getY() && y <= getY() + getHeight()) {
					dragged = true;
				}
			}
	
			@Override
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				super.dragStop(event, x, y, pointer);
				dragged = false;
			}
			
			@Override
			public void drag(InputEvent event, float x, float y, int pointer) {
				if (dragged) {
					float newX = x - getWidth() /2;
					if(newX >= 0 && newX +getWidth() <= Gdx.graphics.getWidth()) {
						setX(x - getWidth()/2);
					}
				}
			}		
		};
		return onDrag;
	}

	public void accept(EngineActor sender, Visitor v) {
		v.collision(sender, this);
	}
	
	/**
	  * Manage Collision with Homer
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, Homer homer) {}
	
	/**
	  * Manage Collision with Donut
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, Donut donut) {}
	
	/**
	  * Manage Collision with PoisonDonut
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, PoisonDonut poisonDonut) {}
	
	
}
