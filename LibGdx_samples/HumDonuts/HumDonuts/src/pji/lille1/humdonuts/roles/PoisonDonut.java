package pji.lille1.humdonuts.roles;

import pji.lille1.humdonuts.PixelEntity;
import pji.lille1.humdonuts.actors.EngineActor;
import pji.lille1.humdonuts.roles.collisions.Visitor;
import pji.lille1.humdonuts.screens.LooseScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class PoisonDonut extends PixelEntity {
	private static Texture texture;
	private static TextureRegion textureRegion;
	

	public PoisonDonut() {
		super(textureRegion);
		
	}

	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		
	}

	public void act(float delta) {
		super.act(delta);
		
	}

	public static void dispose() {
		texture.dispose();
		texture = null;
		textureRegion = null;
	}

	public static void init() {
		if(texture == null) {
			texture = new Texture(Gdx.files.internal("data/actors/images/poisonDonut.png"));
			textureRegion = new TextureRegion(texture);
		}
	}

	public void move(){
		this.setY(getY() - 5);
	}

	

	public void accept(EngineActor sender, Visitor v) {
		v.collision(sender, this);
	}
	
	/**
	  * Manage Collision with Donut
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, Donut donut) {}
	
	/**
	  * Manage Collision with PoisonDonut
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, PoisonDonut poisonDonut) {}
	
	/**
	  * Manage Collision with Homer
	  * Do nothing when this collision is happening
	  */
	public void collision(EngineActor sender, Homer homer) {
		Game game = sender.getGame(); 
		game.setScreen(new LooseScreen(game));
	}
	
}
