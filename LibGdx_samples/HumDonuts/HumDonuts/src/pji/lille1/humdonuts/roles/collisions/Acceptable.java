package pji.lille1.humdonuts.roles.collisions;

import pji.lille1.humdonuts.actors.EngineActor;

public interface Acceptable {
	void accept(EngineActor sender, Visitor v);
}
