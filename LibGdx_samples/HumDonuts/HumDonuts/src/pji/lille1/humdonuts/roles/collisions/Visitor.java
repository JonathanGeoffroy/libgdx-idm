package pji.lille1.humdonuts.roles.collisions;
import pji.lille1.humdonuts.actors.EngineActor;
import pji.lille1.humdonuts.roles.Donut;
import pji.lille1.humdonuts.roles.Homer;
import pji.lille1.humdonuts.roles.PoisonDonut;

public interface Visitor {
	void collision(EngineActor sender, Homer homer);
	void collision(EngineActor sender, Donut donut);
	void collision(EngineActor sender, PoisonDonut poisonDonut);
}
