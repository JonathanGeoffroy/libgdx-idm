package pji.lille1.humdonuts.screens;

import pji.lille1.humdonuts.actors.LooseTable;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;


public class LooseScreen implements Screen {
	/**
	  * A reference to the game which is running
	  */
	private Game game;
	
	/**
	  * The stage which contains all Actors of this Screen
	  */
	private Stage stage;
	
	/**
	  * The loaded texture of the wallpaper
	  */
	private Texture wallpaperTexture;
	
	/**
	  * An actor which points the <wallpaperTexture> to draw this wallpaper using the stage
	  */
	private Image wallpaperActor;
	
	

	public LooseScreen(Game game) {
		super();
		this.game = game;
		this.stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), true);
	}

	@Override
	public void show() {
		/*
		 * Load the wallpaper, and prepare an Image Actor to show it when this screen will be refreshed
		 */
		wallpaperTexture = new Texture(Gdx.files.internal("data/actors/images/menuDonuts.png"));
		TextureRegion wallpaperRegion = new TextureRegion(wallpaperTexture, 1024, 640);
		wallpaperActor = new Image(wallpaperRegion);
		wallpaperActor.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(wallpaperActor);
		
		
		
		Actor currentActor;
		currentActor = new LooseTable(this);
		((LooseTable) currentActor).show();
		// CENTER
		currentActor.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(currentActor);
		
	
		/*
	     * The stage will catch all player events
	     */
		Gdx.input.setInputProcessor(stage);
	}

	/**
	  * Free the memory from all ressources loaded by show()
	  * This method is automatically called by LibGdx internal system when the game is paused.
	  * For example, on tablets, this method's called when the user-player swicth from this game to another Android App
	  */
	@Override
	public void dispose() {
		wallpaperTexture.dispose();
		
		stage.dispose();
	}

	public void render(float delta){
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();;
	}

		@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
		show();		
	}

	public Stage getStage() {
		return stage;
	}
	
	public Game getGame() {
		return game;
	}
}
